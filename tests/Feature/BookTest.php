<?php

namespace Tests\Feature;

use App\Actions\ScheduleStatus;
use App\Actions\UserType;
use App\Models\Contributor;
use App\Models\Schedule;
use App\Models\Sme;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class BookTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    private $schedule;
    /**
     * @var Collection|Model
     */
    private $sme;


    public function setUp(): void
    {
        parent::setUp();

        $this->schedule = factory(Schedule::class)->create();

        $this->sme = factory(Sme::class)->create();
        $user = $this->sme->user;
        Role::create([
            'id'   => (string)Str::uuid(),
            'name' => UserType::SME,
        ]);
        $user->assignRole(UserType::SME);
    }

    public function test_sme_can_show_bookings()
    {
        $this->actingAs($this->sme->user);

        $response = $this->get('/bookings');
        $response->assertStatus(200);
        $response->assertViewIs('bookings.index');
    }

    public function test_sme_can_book_schedule()
    {
        $this->actingAs($this->sme->user);

        $response = $this->get('/bookings/' . $this->schedule->id);
        $response->assertStatus(200);
        $response->assertViewIs('bookings.create');

        $booking  = [
            'schedule_id' => $this->schedule->id,
            'reason'      => 'test booking',
        ];
        $response = $this->post(route('schedules.store_book'), $booking);

        $this->assertDatabaseHas('bookings', [
            'sme_id'      => $this->sme->id,
            'schedule_id' => $this->schedule->id,
        ]);
        $this->assertDatabaseHas('schedules', [
            'id'     => $this->schedule->id,
            'status' => ScheduleStatus::BOOKED,
        ]);

    }


}
