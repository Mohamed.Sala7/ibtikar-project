<?php

namespace Tests\Feature;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;


    public function test_show_login_form()
    {
        $response = $this->get('login');
        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    public function test_user_can_login()
    {
        $user = factory(User::class)->create();

        $this->assertDatabaseHas('users', [
            'name'  => $user->name,
            'email' => $user->email,
        ]);

        $response = $this->post('/login', [
            'email'    => $user->email,
            'password' => 'password'
        ]);
        $response->assertRedirect(RouteServiceProvider::HOME);

        $this->assertAuthenticatedAs($user);
    }

}
