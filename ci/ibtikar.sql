-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: localhost    Database: dahabawy
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `app_settings`
--

DROP TABLE IF EXISTS `app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `app_settings_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_settings`
--

LOCK TABLES `app_settings` WRITE;
/*!40000 ALTER TABLE `app_settings` DISABLE KEYS */;
INSERT INTO `app_settings` VALUES (7,'date_format','l jS F Y (H:i:s)'),(8,'language','en'),(17,'is_human_date_format','1'),(18,'app_name','Dahabawy Club'),(19,'app_short_description','Get your maony back while visting Dahab'),(20,'mail_driver','smtp'),(21,'mail_host','smtp.hostinger.com'),(22,'mail_port','587'),(23,'mail_username','fooddelivery@smartersvision.com'),(24,'mail_password',''),(25,'mail_encryption','ssl'),(26,'mail_from_address','fooddelivery@smartersvision.com'),(27,'mail_from_name','Smarter Vision'),(30,'timezone','America/Montserrat'),(32,'theme_contrast','light'),(33,'theme_color','info'),(34,'app_logo','5464c4ce-b33d-4fe9-b761-64193c8eb316'),(35,'nav_color','navbar-dark bg-info'),(38,'logo_bg_color','bg-info'),(66,'default_role','admin'),(68,'facebook_app_id','518416208939727'),(69,'facebook_app_secret','93649810f78fa9ca0d48972fee2a75cd'),(71,'twitter_app_id','twitter'),(72,'twitter_app_secret','twitter 1'),(74,'google_app_id','527129559488-roolg8aq110p8r1q952fqa9tm06gbloe.apps.googleusercontent.com'),(75,'google_app_secret','FpIi8SLgc69ZWodk-xHaOrxn'),(77,'enable_google','1'),(78,'enable_facebook','1'),(93,'enable_stripe','1'),(94,'stripe_key','pk_test_pltzOnX3zsUZMoTTTVUL4O41'),(95,'stripe_secret','sk_test_o98VZx3RKDUytaokX4My3a20'),(101,'custom_field_models.0','App\\Models\\User'),(104,'default_tax','10'),(107,'default_currency','$'),(108,'fixed_header','0'),(109,'fixed_footer','0'),(110,'fcm_key','AAAAHMZiAQA:APA91bEb71b5sN5jl-w_mmt6vLfgGY5-_CQFxMQsVEfcwO3FAh4-mk1dM6siZwwR3Ls9U0pRDpm96WN1AmrMHQ906GxljILqgU2ZB6Y1TjiLyAiIUETpu7pQFyicER8KLvM9JUiXcfWK'),(111,'enable_notifications','1'),(112,'paypal_username','sb-z3gdq482047_api1.business.example.com'),(113,'paypal_password','JV2A7G4SEMLMZ565'),(114,'paypal_secret','AbMmSXVaig1ExpY3utVS3dcAjx7nAHH0utrZsUN6LYwPgo7wfMzrV5WZ'),(115,'enable_paypal','1'),(116,'main_color','#0000ff'),(117,'main_dark_color','#c0d5f5'),(118,'second_color','#5d5deb'),(119,'second_dark_color','#0000ff'),(120,'accent_color','#ffa0a0'),(121,'accent_dark_color','#9999aa'),(122,'scaffold_dark_color','#ff0000'),(123,'scaffold_color','#c4c3f4'),(124,'google_maps_key','AIzaSyCy7y2QHkoFVV4LXRUGJf9KllFYs2quscA'),(125,'mobile_language','en'),(126,'app_version','2.2.0'),(127,'enable_version','1'),(128,'default_currency_id','1'),(129,'default_currency_code','USD'),(130,'default_currency_decimal_digits','2'),(131,'default_currency_rounding','0'),(132,'currency_right','0'),(133,'distance_unit','km');
/*!40000 ALTER TABLE `app_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_extras`
--

DROP TABLE IF EXISTS `cart_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_extras` (
  `extra_id` int(10) unsigned NOT NULL,
  `cart_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`extra_id`,`cart_id`),
  KEY `cart_extras_cart_id_foreign` (`cart_id`),
  CONSTRAINT `cart_extras_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `carts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `cart_extras_extra_id_foreign` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_extras`
--

LOCK TABLES `cart_extras` WRITE;
/*!40000 ALTER TABLE `cart_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carts`
--

DROP TABLE IF EXISTS `carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `carts_food_id_foreign` (`food_id`),
  KEY `carts_user_id_foreign` (`user_id`),
  CONSTRAINT `carts_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carts`
--

LOCK TABLES `carts` WRITE;
/*!40000 ALTER TABLE `carts` DISABLE KEYS */;
/*!40000 ALTER TABLE `carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'Hotels','<p><a href=\"https://staff.dahabawy.club/categories/2\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Hotels</a><br></p>','2020-06-15 21:51:03','2020-06-18 16:34:25'),(6,'Resturant','<p><a href=\"https://staff.dahabawy.club/categories/6\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Resturant</a><br></p>','2020-06-18 16:44:32','2020-06-18 16:44:32'),(7,'Diving Center','<p><a href=\"https://staff.dahabawy.club/categories/23\" style=\"background-color: rgb(255, 255, 255); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Diving Center</a><br></p>','2020-06-18 16:44:44','2020-06-18 16:44:44'),(8,'Safari','<p><a href=\"https://staff.dahabawy.club/categories/26\" style=\"background-color: rgb(249, 249, 249); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Safari</a><br></p>','2020-06-18 16:44:57','2020-06-18 16:44:57'),(9,'Spices & Herbs','<p><a href=\"https://staff.dahabawy.club/categories/27\" style=\"background-color: rgb(255, 255, 255); color: rgb(102, 127, 160); outline: none; font-family: \"Open Sans\", sans-serif; font-size: 13px;\">Spices & Herbs</a><br></p>','2020-06-18 16:45:11','2020-06-18 18:01:52'),(10,'Bedouin Clothes','<p><a href=\"https://staff.dahabawy.club/categories/28\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: \"Open Sans\", sans-serif; font-size: 13px;\">Bedouin Clothes</a><br></p>','2020-06-18 16:45:32','2020-06-18 18:02:30'),(11,'Handmade & Souvenirs','<p><a href=\"https://staff.dahabawy.club/categories/29\" style=\"background-color: rgb(255, 255, 255); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Handmade &amp; Souvenirs</a><br></p>','2020-06-18 16:45:46','2020-06-18 16:45:46'),(12,'Snorkling & Free Diving','<p><a href=\"https://staff.dahabawy.club/categories/30\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Snorkling &amp; Free Diving</a><br></p>','2020-06-18 16:46:00','2020-06-18 16:46:00'),(13,'Dahab T-Shirts','<p><a href=\"https://staff.dahabawy.club/categories/31\" style=\"background-color: rgb(255, 255, 255); color: rgb(42, 53, 66); outline: none; font-family: \"Open Sans\", sans-serif; font-size: 13px;\">Dahab T-Shirts</a><br></p>','2020-06-18 16:46:13','2020-06-18 18:09:07'),(14,'Rentals','<p><a href=\"https://staff.dahabawy.club/categories/32\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Rentals</a><br></p>','2020-06-18 16:46:25','2020-06-18 16:46:25'),(15,'Diving Trips','<p><a href=\"https://staff.dahabawy.club/categories/38\" style=\"background-color: rgb(255, 255, 255); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Diving Trips</a><br></p>','2020-06-18 16:46:39','2020-06-18 16:46:39'),(16,'Dessert Trips','<p><a href=\"https://staff.dahabawy.club/categories/39\" style=\"background-color: rgb(249, 249, 249); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Dessert Trips</a><br></p>','2020-06-18 16:46:51','2020-06-18 16:46:51'),(17,'Rentals','<p>Rentals<br></p>','2020-06-18 18:09:39','2020-06-18 18:09:39'),(18,'Diving Trips','<p>Diving Trips<br></p>','2020-06-18 18:10:13','2020-06-18 18:10:13'),(19,'Dessert Trips','<p>Dessert Trips<br></p>','2020-06-18 18:10:45','2020-06-18 18:10:45');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisines`
--

DROP TABLE IF EXISTS `cuisines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisines`
--

LOCK TABLES `cuisines` WRITE;
/*!40000 ALTER TABLE `cuisines` DISABLE KEYS */;
INSERT INTO `cuisines` VALUES (7,'Hotels','<p><a href=\"https://staff.dahabawy.club/categories/2\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: \"Open Sans\", sans-serif; font-size: 13px;\">Hotels</a><br></p>','2020-06-18 17:57:18','2020-06-18 17:58:20'),(8,'Water Sports','<p><a href=\"https://staff.dahabawy.club/categories/4\" style=\"background-color: rgb(255, 255, 255); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Water Sports</a><br></p>','2020-06-18 17:58:55','2020-06-18 17:58:55'),(9,'Resturant','<p>Resturant<br></p>','2020-06-18 17:59:32','2020-06-18 17:59:32'),(10,'Diving Center','<p><a href=\"https://staff.dahabawy.club/categories/23\" style=\"background-color: rgb(255, 255, 255); color: rgb(102, 127, 160); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Diving Center</a><br></p>','2020-06-18 18:00:05','2020-06-18 18:00:05'),(11,'Safari','<p><a href=\"https://staff.dahabawy.club/categories/26\" style=\"background-color: rgb(249, 249, 249); color: rgb(42, 53, 66); outline: none; font-family: &quot;Open Sans&quot;, sans-serif; font-size: 13px;\">Safari</a><br></p>','2020-06-18 18:00:46','2020-06-18 18:00:46');
/*!40000 ALTER TABLE `cuisines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(63) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `decimal_digits` tinyint(3) unsigned DEFAULT NULL,
  `rounding` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
INSERT INTO `currencies` VALUES (1,'US Dollar','$','USD',2,0,'2019-10-22 15:50:48','2019-10-22 15:50:48'),(2,'Euro','€','EUR',2,0,'2019-10-22 15:51:39','2019-10-22 15:51:39'),(3,'Indian Rupee','টকা','INR',2,0,'2019-10-22 15:52:50','2019-10-22 15:52:50'),(4,'Indonesian Rupiah','Rp','IDR',0,0,'2019-10-22 15:53:22','2019-10-22 15:53:22'),(5,'Brazilian Real','R$','BRL',2,0,'2019-10-22 15:54:00','2019-10-22 15:54:00'),(6,'Cambodian Riel','៛','KHR',2,0,'2019-10-22 15:55:51','2019-10-22 15:55:51'),(7,'Vietnamese Dong','₫','VND',0,0,'2019-10-22 15:56:26','2019-10-22 15:56:26');
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_field_values`
--

DROP TABLE IF EXISTS `custom_field_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `view` longtext COLLATE utf8mb4_unicode_ci,
  `custom_field_id` int(10) unsigned NOT NULL,
  `customizable_type` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customizable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `custom_field_values_custom_field_id_foreign` (`custom_field_id`),
  CONSTRAINT `custom_field_values_custom_field_id_foreign` FOREIGN KEY (`custom_field_id`) REFERENCES `custom_fields` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_field_values`
--

LOCK TABLES `custom_field_values` WRITE;
/*!40000 ALTER TABLE `custom_field_values` DISABLE KEYS */;
INSERT INTO `custom_field_values` VALUES (29,'+136 226 5669','+136 226 5669',4,'App\\Models\\User',2,'2019-09-06 21:52:30','2019-09-06 21:52:30'),(30,'Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.','Lobortis mattis aliquam faucibus purus. Habitasse platea dictumst vestibulum rhoncus est pellentesque elit. Nunc vel risus commodo viverra maecenas accumsan lacus vel.',5,'App\\Models\\User',2,'2019-09-06 21:52:30','2019-10-16 19:32:35'),(31,'2911 Corpening Drive South Lyon, MI 48178','2911 Corpening Drive South Lyon, MI 48178',6,'App\\Models\\User',2,'2019-09-06 21:52:30','2019-10-16 19:32:35'),(32,'+136 226 5660','+136 226 5660',4,'App\\Models\\User',1,'2019-09-06 21:53:58','2019-09-27 08:12:04'),(33,'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta','Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta',5,'App\\Models\\User',1,'2019-09-06 21:53:58','2019-10-16 19:23:53'),(34,'569 Braxton Street Cortland, IL 60112','569 Braxton Street Cortland, IL 60112',6,'App\\Models\\User',1,'2019-09-06 21:53:58','2019-10-16 19:23:53'),(35,'+1 098-6543-236','+1 098-6543-236',4,'App\\Models\\User',3,'2019-10-15 17:21:32','2019-10-17 23:21:43'),(36,'Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse','Aliquet porttitor lacus luctus accumsan tortor posuere ac ut. Tortor pretium viverra suspendisse',5,'App\\Models\\User',3,'2019-10-15 17:21:32','2019-10-17 23:21:12'),(37,'1850 Big Elm Kansas City, MO 64106','1850 Big Elm Kansas City, MO 64106',6,'App\\Models\\User',3,'2019-10-15 17:21:32','2019-10-17 23:21:43'),(38,'+1 248-437-7610','+1 248-437-7610',4,'App\\Models\\User',4,'2019-10-16 19:31:46','2019-10-16 19:31:46'),(39,'Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta','Faucibus ornare suspendisse sed nisi lacus sed. Pellentesque sit amet porttitor eget dolor morbi non arcu. Eu scelerisque felis imperdiet proin fermentum leo vel orci porta',5,'App\\Models\\User',4,'2019-10-16 19:31:46','2019-10-16 19:31:46'),(40,'1050 Frosty Lane Sidney, NY 13838','1050 Frosty Lane Sidney, NY 13838',6,'App\\Models\\User',4,'2019-10-16 19:31:46','2019-10-16 19:31:46'),(41,'+136 226 5669','+136 226 5669',4,'App\\Models\\User',5,'2019-12-15 18:49:44','2019-12-15 18:49:44'),(42,'<p>Short Bio</p>','Short Bio',5,'App\\Models\\User',5,'2019-12-15 18:49:44','2019-12-15 18:49:44'),(43,'4722 Villa Drive','4722 Villa Drive',6,'App\\Models\\User',5,'2019-12-15 18:49:44','2019-12-15 18:49:44'),(44,'+136 955 6525','+136 955 6525',4,'App\\Models\\User',6,'2020-03-29 17:28:04','2020-03-29 17:28:04'),(45,'<p>Short bio for this driver</p>','Short bio for this driver',5,'App\\Models\\User',6,'2020-03-29 17:28:05','2020-03-29 17:28:05'),(46,'4722 Villa Drive','4722 Villa Drive',6,'App\\Models\\User',6,'2020-03-29 17:28:05','2020-03-29 17:28:05');
/*!40000 ALTER TABLE `custom_field_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(56) COLLATE utf8mb4_unicode_ci NOT NULL,
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disabled` tinyint(1) DEFAULT NULL,
  `required` tinyint(1) DEFAULT NULL,
  `in_table` tinyint(1) DEFAULT NULL,
  `bootstrap_column` tinyint(4) DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  `custom_field_model` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
INSERT INTO `custom_fields` VALUES (4,'phone','text',NULL,0,0,0,6,2,'App\\Models\\User','2019-09-06 21:30:00','2019-09-06 21:31:47'),(5,'bio','textarea',NULL,0,0,0,6,1,'App\\Models\\User','2019-09-06 21:43:58','2019-09-06 21:43:58'),(6,'address','text',NULL,0,0,0,6,3,'App\\Models\\User','2019-09-06 21:49:22','2019-09-06 21:49:22');
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_addresses`
--

DROP TABLE IF EXISTS `delivery_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `delivery_addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `delivery_addresses_user_id_foreign` (`user_id`),
  CONSTRAINT `delivery_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_addresses`
--

LOCK TABLES `delivery_addresses` WRITE;
/*!40000 ALTER TABLE `delivery_addresses` DISABLE KEYS */;
INSERT INTO `delivery_addresses` VALUES (1,'Asperiores nisi dolore consequatur sequi.','33100 Richmond Lakes\nSchummbury, AK 55089-6327','39.23741','7.263452',0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(2,'In labore ipsa saepe facere ut vel sit.','71339 Mikayla Plain\nJulientown, AR 09713-2312','37.375204','11.836102',1,6,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(3,'Sint minima voluptatem expedita nam totam aut.','17546 Kaycee Overpass\nNorth Trevionburgh, NC 56044','51.928686','8.363876',1,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(4,'Velit soluta et voluptatibus maiores autem.','8669 Stone Vista\nCaleville, AR 65794-7679','45.250078','7.63693',1,3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(5,'Nemo minima ab et nulla sint.','8160 Bahringer Skyway Apt. 267\nCaitlynport, FL 73660','53.887005','10.031686',1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(6,'Sint dolor et mollitia aut sequi ratione.','258 Lois Fall Suite 605\nDibbertstad, GA 55965-3553','40.109303','10.65999',0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(7,'Eum dolores harum eveniet rerum est.','37942 Verna Valleys Apt. 729\nKiehnfort, SD 11179','43.182773','7.190246',0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(8,'Et autem suscipit eos deserunt.','8133 Prohaska Plaza Apt. 537\nFlaviefort, MN 36294','37.883332','9.364111',0,3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(9,'Ipsum tempora aspernatur beatae alias.','3225 Dicki Landing Suite 620\nPort Megane, PA 81208-9188','48.695288','9.595259',1,3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(10,'Tempore earum blanditiis et et cumque magnam.','72782 Sawayn Valleys\nGladysfort, OH 19907-1557','38.587009','11.050654',0,6,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(11,'Dolorum nobis eligendi molestias quibusdam eos illum et adipisci.','5806 Vella Landing Suite 859\nBergstromborough, IA 96529-8439','39.080114','10.982684',0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(12,'Exercitationem sed facere officiis distinctio laboriosam reprehenderit cumque.','7407 Reece Ways\nLake Libbyfurt, MI 66874','51.767033','8.696721',1,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(13,'Recusandae optio et vitae officia incidunt quia.','885 Lenore Summit\nSchoenville, CT 67462-8973','37.589439','7.302588',0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(14,'Eum consectetur quisquam et ad vel neque.','99032 Lang Key\nCoopermouth, DE 61096-7354','45.597549','9.682627',1,2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(15,'Quo rem molestiae veritatis nam aliquam saepe.','745 Easter Shoal Suite 546\nMadgemouth, MT 30807-8873','37.830421','11.567941',1,5,'2020-06-15 21:51:03','2020-06-15 21:51:03');
/*!40000 ALTER TABLE `delivery_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `driver_restaurants`
--

DROP TABLE IF EXISTS `driver_restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver_restaurants` (
  `user_id` int(10) unsigned NOT NULL,
  `restaurant_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`restaurant_id`),
  KEY `driver_restaurants_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `driver_restaurants_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `driver_restaurants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver_restaurants`
--

LOCK TABLES `driver_restaurants` WRITE;
/*!40000 ALTER TABLE `driver_restaurants` DISABLE KEYS */;
INSERT INTO `driver_restaurants` VALUES (5,1),(5,2),(6,2),(6,3),(5,4),(6,4);
/*!40000 ALTER TABLE `driver_restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivers`
--

DROP TABLE IF EXISTS `drivers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `delivery_fee` double(5,2) NOT NULL DEFAULT '0.00',
  `total_orders` int(10) unsigned NOT NULL DEFAULT '0',
  `earning` double(9,2) NOT NULL DEFAULT '0.00',
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drivers_user_id_foreign` (`user_id`),
  CONSTRAINT `drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivers`
--

LOCK TABLES `drivers` WRITE;
/*!40000 ALTER TABLE `drivers` DISABLE KEYS */;
INSERT INTO `drivers` VALUES (1,5,0.00,1,0.00,0,'2020-06-15 22:20:45','2020-06-16 17:49:52'),(2,6,0.00,0,0.00,0,'2020-06-15 22:20:45','2020-06-15 22:20:45');
/*!40000 ALTER TABLE `drivers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `drivers_payouts`
--

DROP TABLE IF EXISTS `drivers_payouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `drivers_payouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT '0.00',
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `drivers_payouts_user_id_foreign` (`user_id`),
  CONSTRAINT `drivers_payouts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `drivers_payouts`
--

LOCK TABLES `drivers_payouts` WRITE;
/*!40000 ALTER TABLE `drivers_payouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `drivers_payouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `earnings`
--

DROP TABLE IF EXISTS `earnings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `earnings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(10) unsigned NOT NULL,
  `total_orders` int(10) unsigned NOT NULL DEFAULT '0',
  `total_earning` double(9,2) NOT NULL DEFAULT '0.00',
  `admin_earning` double(9,2) NOT NULL DEFAULT '0.00',
  `restaurant_earning` double(9,2) NOT NULL DEFAULT '0.00',
  `delivery_fee` double(9,2) NOT NULL DEFAULT '0.00',
  `tax` double(9,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `earnings_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `earnings_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `earnings`
--

LOCK TABLES `earnings` WRITE;
/*!40000 ALTER TABLE `earnings` DISABLE KEYS */;
INSERT INTO `earnings` VALUES (1,11,0,0.00,0.00,0.00,0.00,0.00,'2020-06-19 13:50:26','2020-06-19 13:50:26');
/*!40000 ALTER TABLE `earnings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extra_groups`
--

DROP TABLE IF EXISTS `extra_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extra_groups`
--

LOCK TABLES `extra_groups` WRITE;
/*!40000 ALTER TABLE `extra_groups` DISABLE KEYS */;
INSERT INTO `extra_groups` VALUES (1,'Size','2019-08-31 10:55:28','2019-08-31 10:55:28'),(2,'Taste','2019-10-09 13:26:28','2019-10-09 13:26:28'),(3,'Capacity','2019-10-09 13:26:28','2019-10-09 13:26:28');
/*!40000 ALTER TABLE `extra_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extras`
--

DROP TABLE IF EXISTS `extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) DEFAULT '0.00',
  `food_id` int(10) unsigned NOT NULL,
  `extra_group_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `extras_food_id_foreign` (`food_id`),
  KEY `extras_extra_group_id_foreign` (`extra_group_id`),
  CONSTRAINT `extras_extra_group_id_foreign` FOREIGN KEY (`extra_group_id`) REFERENCES `extra_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `extras_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extras`
--

LOCK TABLES `extras` WRITE;
/*!40000 ALTER TABLE `extras` DISABLE KEYS */;
INSERT INTO `extras` VALUES (8,'2L','Quia quam perspiciatis eaque dolorem.',39.04,5,3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(11,'S','Quo accusantium alias.',28.93,23,2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(12,'5L','Suscipit sint doloribus.',35.58,25,1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(14,'5L','Consectetur illo rem.',42.96,5,1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(15,'Tomato','Saepe eos et sapiente.',47.28,16,2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(16,'Tomato','Maiores velit aut cumque quo maiores.',49.67,5,2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(27,'Oil','Officiis voluptate sint numquam aliquam.',15.47,5,2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(47,'Tuna','Voluptas consequatur voluptatem voluptas modi ut.',48.22,5,3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(49,'Oil','Harum distinctio et sunt rem animi.',39.01,16,1,'2020-06-15 21:51:04','2020-06-15 21:51:04');
/*!40000 ALTER TABLE `extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faq_categories`
--

DROP TABLE IF EXISTS `faq_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faq_categories`
--

LOCK TABLES `faq_categories` WRITE;
/*!40000 ALTER TABLE `faq_categories` DISABLE KEYS */;
INSERT INTO `faq_categories` VALUES (1,'Foods','2019-08-31 12:31:52','2019-08-31 12:31:52'),(2,'Services','2019-08-31 12:32:03','2019-08-31 12:32:03'),(3,'Delivery','2019-08-31 12:32:11','2019-08-31 12:32:11'),(4,'Misc','2019-08-31 12:32:17','2019-08-31 12:32:17');
/*!40000 ALTER TABLE `faq_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci,
  `answer` text COLLATE utf8mb4_unicode_ci,
  `faq_category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `faqs_faq_category_id_foreign` (`faq_category_id`),
  CONSTRAINT `faqs_faq_category_id_foreign` FOREIGN KEY (`faq_category_id`) REFERENCES `faq_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faqs`
--

LOCK TABLES `faqs` WRITE;
/*!40000 ALTER TABLE `faqs` DISABLE KEYS */;
INSERT INTO `faqs` VALUES (1,'Expedita autem eaque reprehenderit molestiae sed. Et dignissimos neque voluptatem cupiditate.','Her first idea was that you never to lose YOUR temper!\' \'Hold your tongue!\' added the Gryphon; and then dipped suddenly down, so suddenly that Alice had not long to doubt, for the White Rabbit.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(2,'Error soluta distinctio quasi aut. Delectus in libero incidunt similique ullam et.','I COULD NOT SWIM--\" you can\'t swim, can you?\' he added, turning to Alice, they all quarrel so dreadfully one can\'t hear oneself speak--and they don\'t seem to dry me at home! Why, I wouldn\'t say.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(3,'Reiciendis officia sunt rem vel molestiae. Nihil soluta voluptas et ratione.','He was an old Crab took the hookah out of the garden: the roses growing on it but tea. \'I don\'t think it\'s at all for any of them. \'I\'m sure those are not attending!\' said the Cat. \'I don\'t believe.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(4,'Corporis sunt dolores doloribus. Sit placeat qui animi culpa illo dolores ipsam.','I should be free of them can explain it,\' said Alice indignantly. \'Let me alone!\' \'Serpent, I say again!\' repeated the Pigeon, but in a voice she had accidentally upset the milk-jug into his cup of.',3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(5,'Expedita temporibus vel molestias. Sed earum minus dolores itaque ipsam eaque.','He got behind Alice as he could go. Alice took up the other, and making quite a long time with the Duchess, as she spoke. (The unfortunate little Bill had left off quarrelling with the other.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(6,'Vero dolor itaque unde aut fuga nisi et. Ut qui qui temporibus qui voluptas est.','Gryphon whispered in a hurry. \'No, I\'ll look first,\' she said, as politely as she went to the jury. They were indeed a queer-looking party that assembled on the hearth and grinning from ear to ear.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(7,'Deleniti est voluptates est ab quae modi sunt sed. Expedita ea nam non sit.','CAN all that green stuff be?\' said Alice. \'Then it wasn\'t trouble enough hatching the eggs,\' said the King, and the blades of grass, but she did it at all; however, she again heard a little bottle.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(8,'Est dignissimos molestiae reiciendis. Et culpa vel aliquid occaecati vero dolorum autem.','Queen said to herself, as she passed; it was good manners for her neck kept getting entangled among the distant sobs of the tea--\' \'The twinkling of the thing yourself, some winter day, I will just.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(9,'Repellat officia et ut vero. Ducimus quidem hic earum minima. Debitis impedit cum dolor quia qui.','TWO little shrieks, and more sounds of broken glass, from which she concluded that it was certainly too much of a well?\' \'Take some more tea,\' the March Hare. \'I didn\'t write it, and fortunately was.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(10,'Rerum temporibus et at nisi minus odit itaque. Omnis nobis expedita sed praesentium.','QUITE as much use in waiting by the Queen ordering off her head!\' Alice glanced rather anxiously at the end.\' \'If you knew Time as well go in ringlets at all; however, she waited patiently. \'Once,\'.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(11,'Voluptatem qui sit porro mollitia. Repellat placeat et odit.','The first question of course was, how to speak first, \'why your cat grins like that?\' \'It\'s a mineral, I THINK,\' said Alice. \'Why?\' \'IT DOES THE BOOTS AND SHOES.\' the Gryphon remarked: \'because they.',3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(12,'Omnis non dolorum quibusdam. Voluptatem sed voluptas est quo. Vel aut ut dolorum.','Please, Ma\'am, is this New Zealand or Australia?\' (and she tried the roots of trees, and I\'ve tried hedges,\' the Pigeon in a low curtain she had brought herself down to look over their heads. She.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(13,'Nisi molestiae itaque nemo culpa velit. Veritatis et ipsa porro sit. Laudantium itaque qui et.','However, I\'ve got to go on. \'And so these three weeks!\' \'I\'m very sorry you\'ve been annoyed,\' said Alice, as the Lory hastily. \'I don\'t think--\' \'Then you should say \"With what porpoise?\"\' \'Don\'t.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(14,'Voluptate in natus provident fuga ex. Eos explicabo consequatur et.','Alice went on growing, and growing, and she had this fit) An obstacle that came between Him, and ourselves, and it. Don\'t let me help to undo it!\' \'I shall be punished for it was out of the Rabbit\'s.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(15,'Facere et qui quae vero. Blanditiis consequuntur facilis in inventore.','Alice did not like to go from here?\' \'That depends a good deal on where you want to see that queer little toss of her sharp little chin. \'I\'ve a right to think,\' said Alice indignantly. \'Let me.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(16,'Ut nobis perferendis eos minima quod. Laudantium nostrum rerum est sint.','What happened to me! I\'LL soon make you dry enough!\' They all returned from him to be full of smoke from one foot to the game. CHAPTER IX. The Mock Turtle said: \'advance twice, set to partners--\'.',3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(17,'Dolores eligendi assumenda placeat commodi. Nemo et aliquid et consequuntur.','Stole the Tarts? The King laid his hand upon her face. \'Wake up, Alice dear!\' said her sister; \'Why, what a delightful thing a bit!\' said the Caterpillar. \'Not QUITE right, I\'m afraid,\' said Alice.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(18,'Odio commodi ipsum est exercitationem. Esse sapiente voluptas et excepturi soluta.','I think I should think!\' (Dinah was the first question, you know.\' \'I DON\'T know,\' said the young lady to see its meaning. \'And just as if he wasn\'t one?\' Alice asked. The Hatter was out of its.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(19,'Hic minus qui voluptatum dolorem neque voluptas. Ducimus doloremque placeat laborum dolore.','There was a large rabbit-hole under the door; so either way I\'ll get into that lovely garden. First, however, she waited for a moment like a Jack-in-the-box, and up I goes like a tunnel for some.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(20,'Magni ut est inventore eum. Debitis rerum porro deserunt eum quisquam ullam. Non aut nihil tenetur.','I WAS when I get it home?\' when it saw Alice. It looked good-natured, she thought: still it was getting very sleepy; \'and they all crowded together at one and then the other, and growing sometimes.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(21,'Laboriosam nesciunt aliquam nam enim unde. Atque sint maiores id quo illum est.','Alice. \'But you\'re so easily offended!\' \'You\'ll get used up.\' \'But what did the archbishop find?\' The Mouse did not like to show you! A little bright-eyed terrier, you know, this sort of use in.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(22,'Quibusdam corporis amet ratione occaecati odit error vitae. Eum aperiam enim in et dolores.','Alice, as she tucked her arm affectionately into Alice\'s, and they walked off together, Alice heard it say to this: so she went on, \'What HAVE you been doing here?\' \'May it please your Majesty,\' he.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(23,'Esse at hic ea impedit exercitationem. Illum et et aut dolor et recusandae quo praesentium.','Caterpillar. This was such a dear little puppy it was!\' said Alice, rather doubtfully, as she couldn\'t answer either question, it didn\'t much matter which way you go,\' said the Dodo replied very.',3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(24,'Quia architecto optio et qui. Et ullam et libero deserunt.','Take your choice!\' The Duchess took her choice, and was suppressed. \'Come, that finished the guinea-pigs!\' thought Alice. \'Now we shall have somebody to talk nonsense. The Queen\'s argument was, that.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(25,'Aperiam ut ut dolores iure. Cumque quia quo aperiam.','And so it was good practice to say it over) \'--yes, that\'s about the whiting!\' \'Oh, as to go with Edgar Atheling to meet William and offer him the crown. William\'s conduct at first she thought it.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(26,'Nam laboriosam reprehenderit similique ea iste. Quis qui nisi iusto. Et maxime ipsum in labore.','Pigeon, but in a deep voice, \'are done with a round face, and large eyes full of tears, but said nothing. \'Perhaps it doesn\'t matter which way you can;--but I must go and live in that case I can.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(27,'Accusamus eos maxime earum. Sed quidem alias itaque qui nisi fugiat.','WAS a curious appearance in the court!\' and the Panther received knife and fork with a sigh: \'he taught Laughing and Grief, they used to do:-- \'How doth the little door, had vanished completely.',1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(28,'Temporibus nostrum deleniti ratione impedit provident. Velit saepe impedit aut qui.','For the Mouse with an anxious look at a king,\' said Alice. \'Off with his tea spoon at the cook, and a Canary called out \'The Queen! The Queen!\' and the White Rabbit read out, at the end of half.',2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(29,'Vel nobis commodi odit est enim necessitatibus mollitia. Tempora aliquam quidem rerum.','Alice, quite forgetting in the last concert!\' on which the March Hare, \'that \"I like what I was sent for.\' \'You ought to have it explained,\' said the Gryphon, the squeaking of the pack, she could.',4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(30,'Quia qui qui maxime. Esse reprehenderit pariatur perferendis voluptatibus temporibus quis fugit.','But her sister on the breeze that followed them, the melancholy words:-- \'Soo--oop of the same thing as \"I eat what I used to it!\' pleaded poor Alice. \'But you\'re so easily offended, you know!\' The.',3,'2020-06-15 21:51:04','2020-06-15 21:51:04');
/*!40000 ALTER TABLE `faqs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorite_extras`
--

DROP TABLE IF EXISTS `favorite_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorite_extras` (
  `extra_id` int(10) unsigned NOT NULL,
  `favorite_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`extra_id`,`favorite_id`),
  KEY `favorite_extras_favorite_id_foreign` (`favorite_id`),
  CONSTRAINT `favorite_extras_extra_id_foreign` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `favorite_extras_favorite_id_foreign` FOREIGN KEY (`favorite_id`) REFERENCES `favorites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorite_extras`
--

LOCK TABLES `favorite_extras` WRITE;
/*!40000 ALTER TABLE `favorite_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorite_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favorites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `food_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `favorites_food_id_foreign` (`food_id`),
  KEY `favorites_user_id_foreign` (`user_id`),
  CONSTRAINT `favorites_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
INSERT INTO `favorites` VALUES (6,18,6,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(9,20,1,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(11,25,3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(14,20,5,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(15,25,4,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(18,18,2,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(20,5,3,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(22,18,5,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(25,16,4,'2020-06-15 21:51:04','2020-06-15 21:51:04');
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_order_extras`
--

DROP TABLE IF EXISTS `food_order_extras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_order_extras` (
  `food_order_id` int(10) unsigned NOT NULL,
  `extra_id` int(10) unsigned NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`food_order_id`,`extra_id`),
  KEY `food_order_extras_extra_id_foreign` (`extra_id`),
  CONSTRAINT `food_order_extras_extra_id_foreign` FOREIGN KEY (`extra_id`) REFERENCES `extras` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `food_order_extras_food_order_id_foreign` FOREIGN KEY (`food_order_id`) REFERENCES `food_orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_order_extras`
--

LOCK TABLES `food_order_extras` WRITE;
/*!40000 ALTER TABLE `food_order_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_order_extras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_orders`
--

DROP TABLE IF EXISTS `food_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `food_id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `food_orders_food_id_foreign` (`food_id`),
  KEY `food_orders_order_id_foreign` (`order_id`),
  CONSTRAINT `food_orders_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `food_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_orders`
--

LOCK TABLES `food_orders` WRITE;
/*!40000 ALTER TABLE `food_orders` DISABLE KEYS */;
INSERT INTO `food_orders` VALUES (1,46.78,1,5,1,'2020-06-16 13:49:16','2020-06-16 13:49:16'),(2,10.70,1,18,1,'2020-06-16 13:49:16','2020-06-16 13:49:16'),(3,46.78,2,5,2,'2020-06-16 14:11:58','2020-06-16 14:11:58');
/*!40000 ALTER TABLE `food_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_reviews`
--

DROP TABLE IF EXISTS `food_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review` text COLLATE utf8mb4_unicode_ci,
  `rate` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `food_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `food_reviews_user_id_foreign` (`user_id`),
  KEY `food_reviews_food_id_foreign` (`food_id`),
  CONSTRAINT `food_reviews_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `food_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_reviews`
--

LOCK TABLES `food_reviews` WRITE;
/*!40000 ALTER TABLE `food_reviews` DISABLE KEYS */;
INSERT INTO `food_reviews` VALUES (2,'The Duchess took no notice of her voice, and the sound of many footsteps, and Alice could see it quite plainly through the glass, and she felt a very small cake, on which the words all coming.',4,6,25,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(7,'I know all the unjust things--\' when his eye chanced to fall a long breath, and said anxiously to herself, (not in a bit.\' \'Perhaps it doesn\'t understand English,\' thought Alice; \'I daresay it\'s a.',1,6,16,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(15,'I have to whisper a hint to Time, and round the court was a long sleep you\'ve had!\' \'Oh, I\'ve had such a nice little histories about children who had been running half an hour or so there were a.',3,4,23,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(28,'White Rabbit interrupted: \'UNimportant, your Majesty means, of course,\' he said to a farmer, you know, as we needn\'t try to find her way out. \'I shall be late!\' (when she thought at first she.',1,5,18,'2020-06-15 21:51:03','2020-06-15 21:51:03');
/*!40000 ALTER TABLE `food_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `foods`
--

DROP TABLE IF EXISTS `foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `discount_price` double(8,2) DEFAULT '0.00',
  `description` text COLLATE utf8mb4_unicode_ci,
  `ingredients` text COLLATE utf8mb4_unicode_ci,
  `package_items_count` double(9,2) DEFAULT '0.00',
  `weight` double(9,2) DEFAULT '0.00',
  `unit` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `featured` tinyint(1) DEFAULT '0',
  `deliverable` tinyint(1) DEFAULT '1',
  `restaurant_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foods_restaurant_id_foreign` (`restaurant_id`),
  KEY `foods_category_id_foreign` (`category_id`),
  CONSTRAINT `foods_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `foods_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `foods`
--

LOCK TABLES `foods` WRITE;
/*!40000 ALTER TABLE `foods` DISABLE KEYS */;
INSERT INTO `foods` VALUES (5,'California Italian Wedding Soup',46.78,0.00,'Et consequatur at ullam dignissimos labore autem recusandae. Soluta laborum consequatur et. Dignissimos voluptatibus tenetur architecto ut pariatur ullam. Velit aut at explicabo modi occaecati.',NULL,4.00,263.12,'L',1,1,2,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(16,'California Italian Wedding Soup',27.17,0.00,'Consequatur non nihil hic quis. Molestias recusandae repellendus harum dolorem tenetur quia. Et sequi magni voluptas aut.',NULL,6.00,451.77,'L',0,0,3,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(18,'Italian Sausage Soup',16.42,10.70,'Nisi rerum a distinctio eos quod molestiae laborum. Repellendus ut doloremque qui hic aut perspiciatis. Non nihil ipsam deserunt at occaecati dolores facilis. Quia repudiandae dicta beatae eaque.',NULL,1.00,471.00,'ml',1,1,2,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(20,'Pizza Montanara',47.36,0.00,'Atque consequatur commodi qui aut sit. Mollitia ipsa eligendi et nam nemo fugiat. Et excepturi saepe repellat mollitia ipsum quisquam. Dolore iste eius nobis rem quo est laudantium.',NULL,3.00,394.42,'g',0,1,9,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(23,'Pizza Montanara',19.39,14.95,'Et consequuntur debitis nemo sapiente pariatur doloremque quia praesentium. Unde aut consequatur sapiente. Et ut ex rerum ut iure. Beatae expedita animi modi debitis mollitia aut dolor.',NULL,6.00,14.67,'g',1,1,1,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(25,'California Italian Wedding Soup',47.24,42.72,'Et voluptas tempora mollitia ipsa quia quo minima. Ut facilis ducimus libero ratione eos. Aliquid neque alias ipsa. Voluptates animi harum doloribus et possimus dicta.',NULL,6.00,247.43,'g',0,1,10,5,'2020-06-15 21:51:03','2020-06-15 21:51:03');
/*!40000 ALTER TABLE `foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci,
  `restaurant_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `galleries_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Voluptas quia voluptatem aspernatur illo.',8,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(2,'Et assumenda velit ducimus pariatur mollitia.',6,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(3,'Cumque accusamus sint aut omnis ipsa.',2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(4,'Neque est cupiditate libero qui.',10,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(5,'Qui odio repudiandae aperiam culpa assumenda quia incidunt velit.',8,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(6,'Atque tempore aut molestiae amet sint accusantium.',2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(7,'Qui sit debitis sed blanditiis dolore dolores.',5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(8,'Ab vero molestias corrupti dicta et sunt id.',9,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(9,'Voluptas est non doloribus laboriosam est molestiae ex.',3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(10,'Fuga quod expedita rem rerum id et.',4,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(11,'Nam officiis dolorum explicabo unde voluptas sit et.',10,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(12,'Consectetur doloremque vero repudiandae in necessitatibus architecto.',1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(13,'Blanditiis hic in corporis dolores et rerum rem magnam.',10,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(14,'Tempore fugit nesciunt dolor illo sint.',3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(15,'Qui omnis distinctio et sit earum ut reiciendis.',2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(16,'Qui tempora voluptatem omnis tempore debitis nemo.',4,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(17,'Voluptatem itaque ut quidem rerum minus facere asperiores et.',9,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(18,'Aut nesciunt eum consequatur illum ratione aut quia.',1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(19,'Rerum consequuntur quia consequuntur.',2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(20,'Eius quis et reiciendis consequatur dolores minus.',8,'2020-06-15 21:51:03','2020-06-15 21:51:03');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `manipulations` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (1,'App\\Models\\Upload',1,'app_logo','iconfinder_Beach_379538','iconfinder_Beach_379538.png','image/png','public',26696,'[]','{\"uuid\":\"23411d14-abe9-42f8-bdac-c0484b82bca7\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',1,'2020-06-16 16:56:36','2020-06-16 16:56:37'),(2,'App\\Models\\Upload',2,'app_logo','beach','beach.svg','image/svg','public',31721,'[]','{\"uuid\":\"595e907a-1a5f-495c-9891-5538ec95dd64\",\"user_id\":1}','[]',2,'2020-06-16 16:56:51','2020-06-16 16:56:51'),(3,'App\\Models\\Upload',3,'app_logo','iconfinder_Beach_379538','iconfinder_Beach_379538.png','image/png','public',26696,'[]','{\"uuid\":\"ee8c7390-2964-46e7-bfeb-9d705fef73fb\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',3,'2020-06-16 17:01:39','2020-06-16 17:01:39'),(4,'App\\Models\\Upload',4,'app_logo','summer-png-41190','summer-png-41190.png','image/png','public',182764,'[]','{\"uuid\":\"96197f59-8429-4a69-b5ee-09dfc3f78b5a\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',4,'2020-06-16 17:01:50','2020-06-16 17:01:50'),(5,'App\\Models\\Upload',5,'app_logo','profile_image','profile_image.png','image/jpeg','public',18460,'[]','{\"uuid\":\"5464c4ce-b33d-4fe9-b761-64193c8eb316\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',5,'2020-06-16 17:02:49','2020-06-16 17:02:49'),(6,'App\\Models\\Upload',6,'image','16072019-11144157486347_2718630511511902_512468166249545728_n','16072019-11144157486347_2718630511511902_512468166249545728_n.jpg','image/jpeg','public',63941,'[]','{\"uuid\":\"869ef36a-b006-40f5-91c7-2c64d5f71245\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',6,'2020-06-18 17:58:19','2020-06-18 17:58:19'),(7,'App\\Models\\Cuisine',7,'image','16072019-11144157486347_2718630511511902_512468166249545728_n','16072019-11144157486347_2718630511511902_512468166249545728_n.jpg','image/jpeg','public',63941,'[]','{\"uuid\":\"869ef36a-b006-40f5-91c7-2c64d5f71245\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',7,'2020-06-18 17:58:20','2020-06-18 17:58:20'),(8,'App\\Models\\Upload',7,'image','16072019-091827c1 (1)','16072019-091827c1-(1).jpg','image/jpeg','public',36534,'[]','{\"uuid\":\"bb083750-a5bc-4eac-bd5f-f3c851c0cb19\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',8,'2020-06-18 17:58:53','2020-06-18 17:58:53'),(9,'App\\Models\\Cuisine',8,'image','16072019-091827c1 (1)','16072019-091827c1-(1).jpg','image/jpeg','public',36534,'[]','{\"uuid\":\"bb083750-a5bc-4eac-bd5f-f3c851c0cb19\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',9,'2020-06-18 17:58:55','2020-06-18 17:58:55'),(10,'App\\Models\\Upload',8,'image','15102017-030634wallpaper-food-drink-cocktail-cake-pasta-pizza-awesome-71','15102017-030634wallpaper-food-drink-cocktail-cake-pasta-pizza-awesome-71.jpg','image/jpeg','public',299069,'[]','{\"uuid\":\"94147e73-aea8-45c5-8c20-17d2fadf6970\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',10,'2020-06-18 17:59:31','2020-06-18 17:59:32'),(11,'App\\Models\\Cuisine',9,'image','15102017-030634wallpaper-food-drink-cocktail-cake-pasta-pizza-awesome-71','15102017-030634wallpaper-food-drink-cocktail-cake-pasta-pizza-awesome-71.jpg','image/jpeg','public',299069,'[]','{\"uuid\":\"94147e73-aea8-45c5-8c20-17d2fadf6970\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',11,'2020-06-18 17:59:33','2020-06-18 17:59:33'),(12,'App\\Models\\Upload',9,'image','15072019-054014dviing-palau-bentprop','15072019-054014dviing-palau-bentprop.jpeg','image/jpeg','public',8166,'[]','{\"uuid\":\"b575ed9c-6ec4-4f13-b1e1-c30ff1e727a1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',12,'2020-06-18 18:00:05','2020-06-18 18:00:05'),(13,'App\\Models\\Cuisine',10,'image','15072019-054014dviing-palau-bentprop','15072019-054014dviing-palau-bentprop.jpeg','image/jpeg','public',8166,'[]','{\"uuid\":\"b575ed9c-6ec4-4f13-b1e1-c30ff1e727a1\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',13,'2020-06-18 18:00:05','2020-06-18 18:00:05'),(14,'App\\Models\\Upload',10,'image','16072019-09212679','16072019-09212679.jpg','image/jpeg','public',68766,'[]','{\"uuid\":\"54e66185-f417-46c4-90c8-98c615080a33\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',14,'2020-06-18 18:00:45','2020-06-18 18:00:45'),(15,'App\\Models\\Cuisine',11,'image','16072019-09212679','16072019-09212679.jpg','image/jpeg','public',68766,'[]','{\"uuid\":\"54e66185-f417-46c4-90c8-98c615080a33\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',15,'2020-06-18 18:00:46','2020-06-18 18:00:46'),(16,'App\\Models\\Upload',11,'image','16072019-09280795','16072019-09280795.jpg','image/jpeg','public',114063,'[]','{\"uuid\":\"fc636859-0a64-4e5e-a744-20b468158c6e\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',16,'2020-06-18 18:01:51','2020-06-18 18:01:51'),(17,'App\\Models\\Category',9,'image','16072019-09280795','16072019-09280795.jpg','image/jpeg','public',114063,'[]','{\"uuid\":\"fc636859-0a64-4e5e-a744-20b468158c6e\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',17,'2020-06-18 18:01:52','2020-06-18 18:01:52'),(18,'App\\Models\\Upload',12,'image','16072019-0932193b','16072019-0932193b.jpg','image/jpeg','public',92302,'[]','{\"uuid\":\"c4b85fff-8f3b-4a5d-9250-07f6f26fb8a4\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',18,'2020-06-18 18:02:28','2020-06-18 18:02:29'),(19,'App\\Models\\Category',10,'image','16072019-0932193b','16072019-0932193b.jpg','image/jpeg','public',92302,'[]','{\"uuid\":\"c4b85fff-8f3b-4a5d-9250-07f6f26fb8a4\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',19,'2020-06-18 18:02:30','2020-06-18 18:02:30'),(20,'App\\Models\\Upload',13,'image','16072019-101740iq-company-dive-now-work-later','16072019-101740iq-company-dive-now-work-later.jpg','image/jpeg','public',46929,'[]','{\"uuid\":\"c6866221-9bce-4b89-a642-475da18be0cd\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',20,'2020-06-18 18:09:05','2020-06-18 18:09:06'),(21,'App\\Models\\Category',13,'image','16072019-101740iq-company-dive-now-work-later','16072019-101740iq-company-dive-now-work-later.jpg','image/jpeg','public',46929,'[]','{\"uuid\":\"c6866221-9bce-4b89-a642-475da18be0cd\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',21,'2020-06-18 18:09:07','2020-06-18 18:09:07'),(22,'App\\Models\\Upload',14,'image','16072019-10313015042071_875139479283916_8175053635673048809_o','16072019-10313015042071_875139479283916_8175053635673048809_o.jpg','image/jpeg','public',111864,'[]','{\"uuid\":\"6cab440c-0cf9-4cc7-a717-7ba1b54004f2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',22,'2020-06-18 18:09:37','2020-06-18 18:09:38'),(23,'App\\Models\\Category',17,'image','16072019-10313015042071_875139479283916_8175053635673048809_o','16072019-10313015042071_875139479283916_8175053635673048809_o.jpg','image/jpeg','public',111864,'[]','{\"uuid\":\"6cab440c-0cf9-4cc7-a717-7ba1b54004f2\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',23,'2020-06-18 18:09:39','2020-06-18 18:09:39'),(24,'App\\Models\\Upload',15,'image','17072019-04274377','17072019-04274377.jpg','image/jpeg','public',51128,'[]','{\"uuid\":\"c1fd022e-3af2-464b-a8a1-51a3b1a48f29\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',24,'2020-06-18 18:10:12','2020-06-18 18:10:12'),(25,'App\\Models\\Category',18,'image','17072019-04274377','17072019-04274377.jpg','image/jpeg','public',51128,'[]','{\"uuid\":\"c1fd022e-3af2-464b-a8a1-51a3b1a48f29\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',25,'2020-06-18 18:10:14','2020-06-18 18:10:14'),(26,'App\\Models\\Upload',16,'image','17072019-0446524x4-jeep-adventure-from','17072019-0446524x4-jeep-adventure-from.jpg','image/jpeg','public',45665,'[]','{\"uuid\":\"204c93d9-5411-45c6-a14e-67af5734be84\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',26,'2020-06-18 18:10:44','2020-06-18 18:10:44'),(27,'App\\Models\\Category',19,'image','17072019-0446524x4-jeep-adventure-from','17072019-0446524x4-jeep-adventure-from.jpg','image/jpeg','public',45665,'[]','{\"uuid\":\"204c93d9-5411-45c6-a14e-67af5734be84\",\"user_id\":1,\"generated_conversions\":{\"thumb\":true,\"icon\":true}}','[]',27,'2020-06-18 18:10:45','2020-06-18 18:10:45');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_05_26_175145_create_permission_tables',1),(4,'2018_06_12_140344_create_media_table',1),(5,'2018_06_13_035117_create_uploads_table',1),(6,'2018_07_17_180731_create_settings_table',1),(7,'2018_07_24_211308_create_custom_fields_table',1),(8,'2018_07_24_211327_create_custom_field_values_table',1),(9,'2019_08_29_213820_create_cuisines_table',1),(10,'2019_08_29_213821_create_restaurants_table',1),(11,'2019_08_29_213825_create_categories_table',1),(12,'2019_08_29_213826_create_extra_groups_table',1),(13,'2019_08_29_213829_create_faq_categories_table',1),(14,'2019_08_29_213833_create_order_statuses_table',1),(15,'2019_08_29_213837_create_foods_table',1),(16,'2019_08_29_213842_create_galleries_table',1),(17,'2019_08_29_213847_create_food_reviews_table',1),(18,'2019_08_29_213903_create_nutrition_table',1),(19,'2019_08_29_213907_create_extras_table',1),(20,'2019_08_29_213921_create_payments_table',1),(21,'2019_08_29_213926_create_faqs_table',1),(22,'2019_08_29_213940_create_restaurant_reviews_table',1),(23,'2019_08_30_152927_create_favorites_table',1),(24,'2019_08_31_111103_create_delivery_addresses_table',1),(25,'2019_08_31_111104_create_orders_table',1),(26,'2019_09_04_153857_create_carts_table',1),(27,'2019_09_04_153858_create_favorite_extras_table',1),(28,'2019_09_04_153859_create_cart_extras_table',1),(29,'2019_09_04_153958_create_food_orders_table',1),(30,'2019_09_04_154957_create_food_order_extras_table',1),(31,'2019_09_04_163857_create_user_restaurants_table',1),(32,'2019_10_22_144652_create_currencies_table',1),(33,'2019_12_14_134302_create_driver_restaurants_table',1),(34,'2020_03_25_094752_create_drivers_table',1),(35,'2020_03_25_094802_create_earnings_table',1),(36,'2020_03_25_094809_create_drivers_payouts_table',1),(37,'2020_03_25_094817_create_restaurants_payouts_table',1),(38,'2020_03_27_094855_create_notifications_table',1),(39,'2020_04_11_135804_create_restaurant_cuisines_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` VALUES (2,'App\\Models\\User',1),(3,'App\\Models\\User',2),(4,'App\\Models\\User',3),(4,'App\\Models\\User',4),(5,'App\\Models\\User',5),(5,'App\\Models\\User',6);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES ('0dee47d4-7cfc-48e2-9495-83aa212fbdda','App\\Notifications\\NewOrder','App\\Models\\User',1,'{\"order_id\":1}',NULL,'2020-06-16 13:49:18','2020-06-16 13:49:18'),('22ea72ab-deca-4352-96d3-093a1293f186','App\\Notifications\\StatusChangedOrder','App\\Models\\User',3,'{\"order_id\":1}',NULL,'2020-06-16 17:49:52','2020-06-16 17:49:52'),('5c498a51-4801-499e-943b-fa983dd44e7c','App\\Notifications\\NewOrder','App\\Models\\User',1,'{\"order_id\":2}',NULL,'2020-06-16 14:11:58','2020-06-16 14:11:58'),('8ad82b5b-433c-45a8-919a-df82462b846d','App\\Notifications\\StatusChangedOrder','App\\Models\\User',3,'{\"order_id\":3}',NULL,'2020-06-16 17:48:55','2020-06-16 17:48:55'),('edce8956-6a53-43d7-8b7e-b80d87496175','App\\Notifications\\AssignedOrder','App\\Models\\User',5,'{\"order_id\":1}',NULL,'2020-06-16 17:49:52','2020-06-16 17:49:52');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nutrition`
--

DROP TABLE IF EXISTS `nutrition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nutrition` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(10) unsigned DEFAULT '0',
  `food_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nutrition_food_id_foreign` (`food_id`),
  CONSTRAINT `nutrition_food_id_foreign` FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nutrition`
--

LOCK TABLES `nutrition` WRITE;
/*!40000 ALTER TABLE `nutrition` DISABLE KEYS */;
INSERT INTO `nutrition` VALUES (3,'Calcium',136,18,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(10,'Proteins',90,23,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(13,'Lipid',199,20,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(17,'Lipid',43,23,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(19,'Proteins',170,18,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(33,'Lipid',36,20,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(36,'Calcium',51,23,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(38,'Proteins',129,20,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(39,'Calcium',66,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(45,'Calcium',131,18,'2020-06-15 21:51:04','2020-06-15 21:51:04'),(50,'Calcium',92,18,'2020-06-15 21:51:04','2020-06-15 21:51:04');
/*!40000 ALTER TABLE `nutrition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_statuses`
--

DROP TABLE IF EXISTS `order_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_statuses`
--

LOCK TABLES `order_statuses` WRITE;
/*!40000 ALTER TABLE `order_statuses` DISABLE KEYS */;
INSERT INTO `order_statuses` VALUES (1,'Order Received','2019-08-30 16:39:28','2019-10-15 18:03:14'),(2,'Preparing','2019-10-15 18:03:50','2019-10-15 18:03:50'),(3,'Ready','2019-10-15 18:04:30','2019-10-15 18:04:30'),(4,'On the Way','2019-10-15 18:04:13','2019-10-15 18:04:13'),(5,'Delivered','2019-10-15 18:04:30','2019-10-15 18:04:30');
/*!40000 ALTER TABLE `order_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `order_status_id` int(10) unsigned NOT NULL,
  `tax` double(5,2) DEFAULT '0.00',
  `delivery_fee` double(5,2) DEFAULT '0.00',
  `hint` text COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `driver_id` int(10) unsigned DEFAULT NULL,
  `delivery_address_id` int(10) unsigned DEFAULT NULL,
  `payment_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_user_id_foreign` (`user_id`),
  KEY `orders_order_status_id_foreign` (`order_status_id`),
  KEY `orders_driver_id_foreign` (`driver_id`),
  KEY `orders_delivery_address_id_foreign` (`delivery_address_id`),
  KEY `orders_payment_id_foreign` (`payment_id`),
  CONSTRAINT `orders_delivery_address_id_foreign` FOREIGN KEY (`delivery_address_id`) REFERENCES `delivery_addresses` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `orders_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orders_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,3,5,17.64,0.00,NULL,1,5,NULL,1,'2020-06-16 13:49:16','2020-06-16 17:49:52'),(2,3,1,17.64,7.21,NULL,1,NULL,4,2,'2020-06-16 14:11:58','2020-06-16 14:11:58'),(3,3,2,10.15,0.00,NULL,1,NULL,4,3,'2020-06-16 17:15:24','2020-06-16 17:48:55');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` double(8,2) NOT NULL DEFAULT '0.00',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `payments_user_id_foreign` (`user_id`),
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,67.62,'Order not paid yet',3,'Paid','Pay on Pickup','2020-06-16 13:49:16','2020-06-16 17:49:52'),(2,118.55,'Order not paid yet',3,'Waiting for Client','Cash on Delivery','2020-06-16 14:11:58','2020-06-16 14:11:58'),(3,94.13,'Order not paid yet',3,'Paid','Pay on Pickup','2020-06-16 17:15:24','2020-06-16 17:48:56');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'users.profile','web','2020-03-29 14:58:02','2020-03-29 14:58:02',NULL),(2,'dashboard','web','2020-03-29 14:58:02','2020-03-29 14:58:02',NULL),(3,'medias.create','web','2020-03-29 14:58:02','2020-03-29 14:58:02',NULL),(4,'medias.delete','web','2020-03-29 14:58:02','2020-03-29 14:58:02',NULL),(5,'medias','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(6,'permissions.index','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(7,'permissions.edit','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(8,'permissions.update','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(9,'permissions.destroy','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(10,'roles.index','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(11,'roles.edit','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(12,'roles.update','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(13,'roles.destroy','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(14,'customFields.index','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(15,'customFields.create','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(16,'customFields.store','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(17,'customFields.show','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(18,'customFields.edit','web','2020-03-29 14:58:03','2020-03-29 14:58:03',NULL),(19,'customFields.update','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(20,'customFields.destroy','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(21,'users.login-as-user','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(22,'users.index','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(23,'users.create','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(24,'users.store','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(25,'users.show','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(26,'users.edit','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(27,'users.update','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(28,'users.destroy','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(29,'app-settings','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(30,'restaurants.index','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(31,'restaurants.create','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(32,'restaurants.store','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(33,'restaurants.edit','web','2020-03-29 14:58:04','2020-03-29 14:58:04',NULL),(34,'restaurants.update','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(35,'restaurants.destroy','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(36,'categories.index','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(37,'categories.create','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(38,'categories.store','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(39,'categories.edit','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(40,'categories.update','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(41,'categories.destroy','web','2020-03-29 14:58:05','2020-03-29 14:58:05',NULL),(42,'faqCategories.index','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(43,'faqCategories.create','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(44,'faqCategories.store','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(45,'faqCategories.edit','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(46,'faqCategories.update','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(47,'faqCategories.destroy','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(48,'orderStatuses.index','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(49,'orderStatuses.show','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(50,'orderStatuses.edit','web','2020-03-29 14:58:06','2020-03-29 14:58:06',NULL),(51,'orderStatuses.update','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(52,'foods.index','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(53,'foods.create','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(54,'foods.store','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(55,'foods.edit','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(56,'foods.update','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(57,'foods.destroy','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(58,'galleries.index','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(59,'galleries.create','web','2020-03-29 14:58:07','2020-03-29 14:58:07',NULL),(60,'galleries.store','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(61,'galleries.edit','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(62,'galleries.update','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(63,'galleries.destroy','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(64,'foodReviews.index','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(65,'foodReviews.create','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(66,'foodReviews.store','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(67,'foodReviews.edit','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(68,'foodReviews.update','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(69,'foodReviews.destroy','web','2020-03-29 14:58:08','2020-03-29 14:58:08',NULL),(76,'extras.index','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(77,'extras.create','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(78,'extras.store','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(79,'extras.show','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(80,'extras.edit','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(81,'extras.update','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(82,'extras.destroy','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(83,'payments.index','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(84,'payments.show','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(85,'payments.update','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(86,'faqs.index','web','2020-03-29 14:58:10','2020-03-29 14:58:10',NULL),(87,'faqs.create','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(88,'faqs.store','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(89,'faqs.edit','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(90,'faqs.update','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(91,'faqs.destroy','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(92,'restaurantReviews.index','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(93,'restaurantReviews.create','web','2020-03-29 14:58:11','2020-03-29 14:58:11',NULL),(94,'restaurantReviews.store','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(95,'restaurantReviews.edit','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(96,'restaurantReviews.update','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(97,'restaurantReviews.destroy','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(98,'favorites.index','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(99,'favorites.create','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(100,'favorites.store','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(101,'favorites.edit','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(102,'favorites.update','web','2020-03-29 14:58:12','2020-03-29 14:58:12',NULL),(103,'favorites.destroy','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(104,'orders.index','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(105,'orders.create','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(106,'orders.store','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(107,'orders.show','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(108,'orders.edit','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(109,'orders.update','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(110,'orders.destroy','web','2020-03-29 14:58:13','2020-03-29 14:58:13',NULL),(111,'notifications.index','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(112,'notifications.show','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(113,'notifications.destroy','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(114,'carts.index','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(115,'carts.edit','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(116,'carts.update','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(117,'carts.destroy','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(118,'currencies.index','web','2020-03-29 14:58:14','2020-03-29 14:58:14',NULL),(119,'currencies.create','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(120,'currencies.store','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(121,'currencies.edit','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(122,'currencies.update','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(123,'currencies.destroy','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(124,'deliveryAddresses.index','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(125,'deliveryAddresses.create','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(126,'deliveryAddresses.store','web','2020-03-29 14:58:15','2020-03-29 14:58:15',NULL),(127,'deliveryAddresses.edit','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(128,'deliveryAddresses.update','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(129,'deliveryAddresses.destroy','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(130,'drivers.index','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(131,'drivers.create','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(132,'drivers.store','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(133,'drivers.show','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(134,'drivers.edit','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(135,'drivers.update','web','2020-03-29 14:58:16','2020-03-29 14:58:16',NULL),(136,'drivers.destroy','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(137,'earnings.index','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(138,'earnings.create','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(139,'earnings.store','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(140,'earnings.show','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(141,'earnings.edit','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(142,'earnings.update','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(143,'earnings.destroy','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(144,'driversPayouts.index','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(145,'driversPayouts.create','web','2020-03-29 14:58:17','2020-03-29 14:58:17',NULL),(146,'driversPayouts.store','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(147,'driversPayouts.show','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(148,'driversPayouts.edit','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(149,'driversPayouts.update','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(150,'driversPayouts.destroy','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(151,'restaurantsPayouts.index','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(152,'restaurantsPayouts.create','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(153,'restaurantsPayouts.store','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(154,'restaurantsPayouts.show','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(155,'restaurantsPayouts.edit','web','2020-03-29 14:58:18','2020-03-29 14:58:18',NULL),(156,'restaurantsPayouts.update','web','2020-03-29 14:58:19','2020-03-29 14:58:19',NULL),(157,'restaurantsPayouts.destroy','web','2020-03-29 14:58:19','2020-03-29 14:58:19',NULL),(158,'permissions.create','web','2020-03-29 14:59:15','2020-03-29 14:59:15',NULL),(159,'permissions.store','web','2020-03-29 14:59:15','2020-03-29 14:59:15',NULL),(160,'permissions.show','web','2020-03-29 14:59:15','2020-03-29 14:59:15',NULL),(161,'roles.create','web','2020-03-29 14:59:15','2020-03-29 14:59:15',NULL),(162,'roles.store','web','2020-03-29 14:59:15','2020-03-29 14:59:15',NULL),(163,'roles.show','web','2020-03-29 14:59:16','2020-03-29 14:59:16',NULL),(164,'cuisines.index','web','2020-04-11 15:04:39','2020-04-11 15:04:39',NULL),(165,'cuisines.create','web','2020-04-11 15:04:39','2020-04-11 15:04:39',NULL),(166,'cuisines.store','web','2020-04-11 15:04:39','2020-04-11 15:04:39',NULL),(167,'cuisines.edit','web','2020-04-11 15:04:39','2020-04-11 15:04:39',NULL),(168,'cuisines.update','web','2020-04-11 15:04:39','2020-04-11 15:04:39',NULL),(169,'cuisines.destroy','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(170,'extraGroups.index','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(171,'extraGroups.create','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(172,'extraGroups.store','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(173,'extraGroups.edit','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(174,'extraGroups.update','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(175,'extraGroups.destroy','web','2020-04-11 15:04:40','2020-04-11 15:04:40',NULL),(176,'nutrition.index','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(177,'nutrition.create','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(178,'nutrition.store','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(179,'nutrition.edit','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(180,'nutrition.update','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL),(181,'nutrition.destroy','web','2020-03-29 14:58:09','2020-03-29 14:58:09',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_cuisines`
--

DROP TABLE IF EXISTS `restaurant_cuisines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_cuisines` (
  `cuisine_id` int(10) unsigned NOT NULL,
  `restaurant_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`cuisine_id`,`restaurant_id`),
  KEY `restaurant_cuisines_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `restaurant_cuisines_cuisine_id_foreign` FOREIGN KEY (`cuisine_id`) REFERENCES `cuisines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `restaurant_cuisines_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_cuisines`
--

LOCK TABLES `restaurant_cuisines` WRITE;
/*!40000 ALTER TABLE `restaurant_cuisines` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurant_cuisines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_reviews`
--

DROP TABLE IF EXISTS `restaurant_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `review` text COLLATE utf8mb4_unicode_ci,
  `rate` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `restaurant_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurant_reviews_user_id_foreign` (`user_id`),
  KEY `restaurant_reviews_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `restaurant_reviews_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `restaurant_reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_reviews`
--

LOCK TABLES `restaurant_reviews` WRITE;
/*!40000 ALTER TABLE `restaurant_reviews` DISABLE KEYS */;
INSERT INTO `restaurant_reviews` VALUES (1,'March Hare moved into the sky. Twinkle, twinkle--\"\' Here the other side. The further off from England the nearer is to give the hedgehog to, and, as the hall was very deep, or she fell very slowly.',3,2,7,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(2,'And yet I don\'t believe there\'s an atom of meaning in it.\' The jury all brightened up at this moment Five, who had followed him into the darkness as hard as it went, as if he doesn\'t begin.\' But she.',2,5,2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(3,'WOULD always get into her head. Still she went to school in the distance, sitting sad and lonely on a summer day: The Knave of Hearts, who only bowed and smiled in reply. \'Idiot!\' said the Lory.',2,2,2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(4,'I could let you out, you know.\' \'Not at first, the two sides of it; and the three gardeners instantly threw themselves flat upon their faces. There was a paper label, with the grin, which remained.',3,6,2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(5,'Rabbit, and had no very clear notion how long ago anything had happened.) So she stood still where she was losing her temper. \'Are you content now?\' said the Mock Turtle. \'Certainly not!\' said Alice.',2,1,6,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(6,'White Rabbit, with a yelp of delight, which changed into alarm in another moment that it would be only rustling in the world she was near enough to try the first question, you know.\' \'Who is it.',4,2,10,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(7,'Alice thought she had found her head was so long since she had caught the flamingo and brought it back, the fight was over, and she could not tell whether they were nowhere to be trampled under its.',3,5,7,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(8,'For anything tougher than suet; Yet you finished the first sentence in her life before, and behind it, it occurred to her very much confused, \'I don\'t think--\' \'Then you keep moving round, I.',2,2,6,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(9,'I get\" is the use of repeating all that green stuff be?\' said Alice. \'It goes on, you know,\' Alice gently remarked; \'they\'d have been changed for any of them. However, on the top of his pocket, and.',2,3,2,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(10,'There ought to be executed for having cheated herself in a tone of great relief. \'Now at OURS they had settled down in a hurry to get through was more than nine feet high. \'Whoever lives there,\'.',4,1,3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(11,'Dormouse,\' the Queen jumped up and said, without opening its eyes, for it flashed across her mind that she looked up, and began whistling. \'Oh, there\'s no room at all fairly,\' Alice began, in a.',4,1,3,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(12,'Bill\'s got to the law, And argued each case with MINE,\' said the March Hare had just begun to think about it, and then said \'The fourth.\' \'Two days wrong!\' sighed the Hatter. He came in sight of the.',5,3,7,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(13,'I wonder?\' And here Alice began telling them her adventures from the change: and Alice was just going to begin at HIS time of life. The King\'s argument was, that anything that had made the whole.',3,1,10,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(14,'How I wonder if I\'ve been changed for Mabel! I\'ll try if I might venture to say it over) \'--yes, that\'s about the whiting!\' \'Oh, as to go down--Here, Bill! the master says you\'re to go near the King.',3,3,7,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(15,'Queen. \'You make me giddy.\' And then, turning to Alice, they all spoke at once, while all the rest, Between yourself and me.\' \'That\'s the judge,\' she said to herself, as usual. I wonder what they\'ll.',4,2,7,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(16,'First, because I\'m on the bank--the birds with draggled feathers, the animals with their fur clinging close to her usual height. It was so ordered about in the sea!\' cried the Mock Turtle recovered.',3,6,4,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(17,'Five, \'and I\'ll tell him--it was for bringing the cook took the cauldron of soup off the fire, stirring a large cat which was immediately suppressed by the hedge!\' then silence, and then said \'The.',3,2,4,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(18,'She generally gave herself very good advice, (though she very soon had to be told so. \'It\'s really dreadful,\' she muttered to herself, \'it would be very likely it can be,\' said the Cat. \'I\'d nearly.',2,4,5,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(19,'I don\'t want to see it trot away quietly into the teapot. \'At any rate a book written about me, that there was the first minute or two sobs choked his voice. \'Same as if she had caught the baby with.',1,4,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(20,'Alice, very earnestly. \'I\'ve had nothing else to do, and in another moment that it felt quite unhappy at the mushroom for a long silence after this, and she at once without waiting for the.',3,2,9,'2020-06-15 21:51:03','2020-06-15 21:51:03');
/*!40000 ALTER TABLE `restaurant_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants`
--

DROP TABLE IF EXISTS `restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `information` text COLLATE utf8mb4_unicode_ci,
  `admin_commission` double(8,2) DEFAULT '0.00',
  `delivery_fee` double(8,2) DEFAULT '0.00',
  `delivery_range` double(8,2) DEFAULT '0.00',
  `default_tax` double(8,2) DEFAULT '0.00',
  `closed` tinyint(1) DEFAULT '0',
  `available_for_delivery` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,'Pizza Brown, Langosh and Kub','Est occaecati sed possimus ut eos molestias asperiores. Animi aut laboriosam tenetur sed. Cumque aut officiis aliquid enim ipsum et. Culpa nesciunt aut architecto.','92890 Price Pines Suite 700\nNorth Nedshire, TX 46083-5270','49.248865','10.368156','+15378972015','428.877.8356 x04105','Eius et dignissimos magni. Ipsa aut placeat id aut voluptatem beatae eaque. Dolor nisi dignissimos rerum voluptas aliquid.',33.38,6.23,84.81,7.93,1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(2,'Restaurant Hermiston, Kunze and Reilly','Ullam sapiente harum magni et. Velit mollitia distinctio commodi sit. Excepturi consectetur unde deserunt consequatur. Et ex in qui.','1082 Flatley Causeway\nKarinaview, WV 61166-7794','40.965068','11.614844','1-558-488-9714 x6390','245.571.6572','Libero eos in ex voluptates commodi. Et veniam rerum sit asperiores exercitationem hic cum. Aliquid fugit quam ex voluptatibus et est.',47.51,7.21,9.49,17.64,0,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(3,'Restaurant Legros LLC','Quaerat excepturi animi quia totam neque. Et eius sunt porro nemo non sapiente. Porro modi eum qui illo consequatur iure repellat. Voluptatibus architecto voluptatem voluptatibus rerum et.','369 Bosco Pines Suite 755\nSouth Evebury, NC 96397-4060','40.856816','10.859705','+1-361-812-1394','1-521-620-8498 x19278','Numquam repudiandae provident repellendus distinctio. Hic aut qui facilis possimus et repellat voluptas ab. Nihil pariatur id saepe laboriosam non voluptas est.',46.49,1.17,89.80,29.00,1,0,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(4,'Burger Friesen-Bins','Illo necessitatibus quia repellat a voluptatem. Voluptas sunt similique doloremque sed dolor aut. Totam doloribus non impedit ipsum voluptates similique voluptatem. Sed saepe nam tenetur dolor.','552 Vincenza Route Suite 634\nTomasmouth, KS 99415','51.341931','9.062672','384.313.9148 x2620','307.481.4153','Sint labore et quasi eos. Et itaque rem et commodi sit. Placeat adipisci quae repudiandae a ratione.',46.22,8.08,78.29,5.15,1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(5,'Meal Deckow, Rodriguez and Wiza','Provident et ipsam et tenetur et repudiandae. Repellendus aut quis sed libero nesciunt repellendus. Optio architecto officia a atque aliquid.','11269 Johnpaul Spring\nEast Dolores, OH 41350-0111','51.818153','8.416582','(737) 353-2721','(292) 893-8959','Quis unde aut sit accusantium similique. Est vel quidem ipsam accusantium. Ea asperiores iusto atque sed.',34.35,2.93,20.50,9.45,1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(6,'Foody Stroman-Eichmann','Dicta et nobis a qui soluta tempore. Quaerat fuga doloribus est voluptas dolor sed. Rem est mollitia nam consequatur totam. Id ab earum illum commodi consectetur itaque similique.','13916 Aimee Inlet Apt. 006\nNicolasport, LA 36266','38.486048','8.750821','+1 (826) 587-3060','690-812-6860','Voluptas ducimus velit accusamus atque. Et consectetur id quibusdam velit alias officia et nihil. Reiciendis ducimus libero assumenda alias eum minus nihil est.',12.92,8.20,40.67,26.28,1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(7,'Meal Hettinger and Sons','Animi a odio et omnis hic delectus. Iure aut et laudantium similique necessitatibus. Qui ad reprehenderit illo blanditiis laboriosam iusto tempore.','934 Heidenreich Circles Apt. 154\nCecilton, VA 49741','49.590179','11.33626','549-471-5025','682-281-6113 x7031','Ut voluptatibus est architecto quo dolorem quia sit. Et vero debitis veniam sunt. Dolorem totam totam aut nostrum.',40.06,6.82,56.91,17.14,1,0,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(8,'Pizza Russel-Wisozk','Ut sint non labore nihil impedit. Omnis explicabo dolorum unde vitae blanditiis et quae. Provident provident perspiciatis facere.','70683 Alaina Heights Suite 456\nWuckertmouth, NC 28736','52.655344','9.610979','574-630-2690','1-368-266-2045','Ad tempore aperiam nulla modi voluptate voluptates. Tempora qui ad eligendi dolor sed et voluptatem. Sed quae voluptatum qui ut sint alias.',15.88,4.49,51.70,10.15,0,0,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(9,'Meal Kuvalis, Koch and Pfannerstill','Quos error eos nesciunt quis sequi. Praesentium aliquam id dolorem quam est est. Aut totam architecto sit nostrum facere molestiae. Placeat quisquam sint tenetur atque.','963 Botsford Pike Suite 977\nNorth Giovanniview, PA 78428-8778','38.031503','11.483789','751-614-7485 x6732','+1-705-787-7053','Repellat consequuntur ea sed voluptatem rerum. Repellat quia deserunt exercitationem officiis voluptatem id. Ex omnis omnis nulla deleniti at aliquam.',47.80,1.44,66.56,8.25,1,0,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(10,'Meal Fadel-Mueller','Tempora porro similique et aut aspernatur. Et doloribus et delectus exercitationem itaque. Est eos dolores voluptatem nulla. Et velit sunt distinctio velit nobis modi.','81355 Reynolds Common\nHamilltown, WA 89735','37.741751','8.545359','435-221-5112','+1-847-593-4151','Aut qui similique similique quibusdam. Nulla magnam non magnam reiciendis. Accusamus voluptatem et vel ex.',19.43,1.37,55.57,7.62,1,1,'2020-06-15 21:51:03','2020-06-15 21:51:03'),(11,'Acacia Hotel, Egypt','<p>ffff<img style=\"width: 558.958px;\" src=\"https://maps.googleapis.com/maps/api/place/js/PhotoService.GetPhoto?1sCmRaAAAAL3lBUb9AJbGIzkj6ZKDfxOzYadTPgtEwQlxI5rbJUgUC93vUnoOydXL0ZcbZUpK7_xcMw9b7ZsJgqfT9iQnGmexFdUE8tt8cYzy25Mh0HKXUq0S8CNE-Odb-Za8ViUmYEhDi691pynOoez9tU7UWlGESGhR9asEVrMccKC97ZgZa5bST8fQICA&3u4160&5m1&2e1&callback=none&key=AIzaSyCy7y2QHkoFVV4LXRUGJf9KllFYs2quscA&token=100311\"></p><p><br></p><p>srfsdfsf</p><p><br></p><p><span style=\"font-family: Verdana;\">ssssssss</span></p>','5 Mashraba Street, جنوب سيناء 46611, Egypt','28.4912714','34.5157234',NULL,NULL,NULL,90.00,NULL,NULL,NULL,1,0,'2020-06-19 13:50:26','2020-06-19 13:50:26');
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants_payouts`
--

DROP TABLE IF EXISTS `restaurants_payouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants_payouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(10) unsigned NOT NULL,
  `method` varchar(127) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` double(9,2) NOT NULL DEFAULT '0.00',
  `paid_date` datetime DEFAULT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `restaurants_payouts_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `restaurants_payouts_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants_payouts`
--

LOCK TABLES `restaurants_payouts` WRITE;
/*!40000 ALTER TABLE `restaurants_payouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `restaurants_payouts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` VALUES (1,2),(2,2),(3,2),(4,2),(5,2),(6,2),(9,2),(10,2),(14,2),(15,2),(16,2),(17,2),(18,2),(19,2),(20,2),(21,2),(22,2),(23,2),(24,2),(26,2),(27,2),(28,2),(29,2),(30,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,2),(37,2),(38,2),(39,2),(40,2),(41,2),(42,2),(43,2),(44,2),(45,2),(46,2),(47,2),(48,2),(50,2),(51,2),(52,2),(53,2),(54,2),(55,2),(56,2),(57,2),(58,2),(59,2),(60,2),(61,2),(62,2),(63,2),(64,2),(67,2),(68,2),(69,2),(76,2),(77,2),(78,2),(80,2),(81,2),(82,2),(83,2),(85,2),(86,2),(87,2),(88,2),(89,2),(90,2),(91,2),(92,2),(95,2),(96,2),(97,2),(98,2),(103,2),(104,2),(107,2),(108,2),(109,2),(110,2),(111,2),(112,2),(113,2),(114,2),(117,2),(118,2),(119,2),(120,2),(121,2),(122,2),(123,2),(124,2),(127,2),(128,2),(129,2),(130,2),(131,2),(134,2),(135,2),(137,2),(138,2),(144,2),(145,2),(146,2),(148,2),(149,2),(151,2),(152,2),(153,2),(155,2),(156,2),(160,2),(164,2),(165,2),(166,2),(167,2),(168,2),(169,2),(170,2),(171,2),(172,2),(173,2),(174,2),(175,2),(176,2),(177,2),(1,3),(3,3),(4,3),(5,3),(27,3),(30,3),(33,3),(34,3),(42,3),(48,3),(52,3),(53,3),(54,3),(55,3),(56,3),(57,3),(58,3),(59,3),(60,3),(61,3),(62,3),(63,3),(64,3),(67,3),(68,3),(76,3),(77,3),(78,3),(80,3),(81,3),(82,3),(83,3),(86,3),(92,3),(95,3),(96,3),(98,3),(103,3),(104,3),(107,3),(108,3),(109,3),(110,3),(111,3),(113,3),(114,3),(117,3),(130,3),(134,3),(135,3),(137,3),(145,3),(146,3),(151,3),(152,3),(153,3),(164,3),(170,3),(171,3),(172,3),(1,4),(3,4),(4,4),(27,4),(30,4),(52,4),(64,4),(67,4),(68,4),(83,4),(86,4),(92,4),(95,4),(96,4),(98,4),(103,4),(104,4),(107,4),(111,4),(113,4),(114,4),(117,4),(164,4),(1,5),(3,5),(4,5),(27,5),(30,5),(48,5),(52,5),(64,5),(67,5),(68,5),(83,5),(86,5),(92,5),(95,5),(96,5),(98,5),(103,5),(104,5),(107,5),(111,5),(113,5),(114,5),(117,5),(130,5),(144,5),(145,5),(146,5),(164,5);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'web',
  `default` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (2,'admin','web',0,'2018-07-21 16:37:56','2019-09-07 22:42:01',NULL),(3,'manager','web',0,'2019-09-07 22:41:38','2019-09-07 22:41:38',NULL),(4,'client','web',1,'2019-09-07 22:41:54','2019-09-07 22:41:54',NULL),(5,'driver','web',0,'2019-12-15 18:50:21','2019-12-15 18:50:21',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploads`
--

DROP TABLE IF EXISTS `uploads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploads`
--

LOCK TABLES `uploads` WRITE;
/*!40000 ALTER TABLE `uploads` DISABLE KEYS */;
INSERT INTO `uploads` VALUES (1,'23411d14-abe9-42f8-bdac-c0484b82bca7','2020-06-16 16:56:36','2020-06-16 16:56:36'),(2,'595e907a-1a5f-495c-9891-5538ec95dd64','2020-06-16 16:56:51','2020-06-16 16:56:51'),(3,'ee8c7390-2964-46e7-bfeb-9d705fef73fb','2020-06-16 17:01:39','2020-06-16 17:01:39'),(4,'96197f59-8429-4a69-b5ee-09dfc3f78b5a','2020-06-16 17:01:50','2020-06-16 17:01:50'),(5,'5464c4ce-b33d-4fe9-b761-64193c8eb316','2020-06-16 17:02:49','2020-06-16 17:02:49'),(6,'869ef36a-b006-40f5-91c7-2c64d5f71245','2020-06-18 17:58:18','2020-06-18 17:58:18'),(7,'bb083750-a5bc-4eac-bd5f-f3c851c0cb19','2020-06-18 17:58:53','2020-06-18 17:58:53'),(8,'94147e73-aea8-45c5-8c20-17d2fadf6970','2020-06-18 17:59:31','2020-06-18 17:59:31'),(9,'b575ed9c-6ec4-4f13-b1e1-c30ff1e727a1','2020-06-18 18:00:05','2020-06-18 18:00:05'),(10,'54e66185-f417-46c4-90c8-98c615080a33','2020-06-18 18:00:45','2020-06-18 18:00:45'),(11,'fc636859-0a64-4e5e-a744-20b468158c6e','2020-06-18 18:01:51','2020-06-18 18:01:51'),(12,'c4b85fff-8f3b-4a5d-9250-07f6f26fb8a4','2020-06-18 18:02:28','2020-06-18 18:02:28'),(13,'c6866221-9bce-4b89-a642-475da18be0cd','2020-06-18 18:09:05','2020-06-18 18:09:05'),(14,'6cab440c-0cf9-4cc7-a717-7ba1b54004f2','2020-06-18 18:09:37','2020-06-18 18:09:37'),(15,'c1fd022e-3af2-464b-a8a1-51a3b1a48f29','2020-06-18 18:10:12','2020-06-18 18:10:12'),(16,'204c93d9-5411-45c6-a14e-67af5734be84','2020-06-18 18:10:44','2020-06-18 18:10:44');
/*!40000 ALTER TABLE `uploads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_restaurants`
--

DROP TABLE IF EXISTS `user_restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_restaurants` (
  `user_id` int(10) unsigned NOT NULL,
  `restaurant_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`restaurant_id`),
  KEY `user_restaurants_restaurant_id_foreign` (`restaurant_id`),
  CONSTRAINT `user_restaurants_restaurant_id_foreign` FOREIGN KEY (`restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_restaurants_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_restaurants`
--

LOCK TABLES `user_restaurants` WRITE;
/*!40000 ALTER TABLE `user_restaurants` DISABLE KEYS */;
INSERT INTO `user_restaurants` VALUES (1,2),(1,3),(2,3),(2,4),(1,5),(1,6);
/*!40000 ALTER TABLE `user_restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` char(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stripe_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `braintree_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paypal_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_api_token_unique` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Michael E. Quinn','admin@demo.com','$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu','PivvPlsQWxPl1bB5KrbKNBuraJit0PrUZekQUgtLyTRuyBq921atFtoR1HuA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'T4PQhFvBcAA7k02f7ejq4I7z7QKKnvxQLV0oqGnuS6Ktz6FdWULrWrzZ3oYn','2018-08-06 22:58:41','2019-09-27 07:49:45'),(2,'Barbara J. Glanz','manager@demo.com','$2y$10$YOn/Xq6vfvi9oaixrtW8QuM2W0mawkLLqIxL.IoGqrsqOqbIsfBNu','tVSfIKRSX2Yn8iAMoUS3HPls84ycS8NAxO2dj2HvePbbr4WHorp4gIFRmFwB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5nysjzVKI4LU92bjRqMUSYdOaIo1EcPC3pIMb6Tcj2KXSUMriGrIQ1iwRdd0','2018-08-14 17:06:28','2019-09-25 22:09:35'),(3,'Charles W. Abeyta','client@demo.com','$2y$10$EBubVy3wDbqNbHvMQwkj3OTYVitL8QnHvh/zV0ICVOaSbALy5dD0K','fXLu7VeYgXDu82SkMxlLPG1mCAXc4EBIx6O5isgYVIKFQiHah0xiOHmzNsBv','dctFwdL6SF6SNEzNdq7y5i:APA91bHKFV0J4yWPM_-oNlwqcXS-YAVMc92fc5QYIwNXlUF427GaLY8kRetebf04sOjfDLit0oZBgTFfNG8DVIqW5FBlVOLwhGTVOiM4fDrAu-P52o98IVCD0MsKpXRNm2AlVOPNWN-y',NULL,NULL,NULL,NULL,NULL,NULL,'V6PIUfd8JdHT2zkraTlnBcRSINZNjz5Ou7N0WtUGRyaTweoaXKpSfij6UhqC','2019-10-12 22:31:26','2020-06-16 10:58:09'),(4,'Robert E. Brock','client1@demo.com','$2y$10$pmdnepS1FhZUMqOaFIFnNO0spltJpziz3j13UqyEwShmLhokmuoei','Czrsk9rwD0c75NUPkzNXM2WvbxYHKj8p0nG29pjKT0PZaTgMVzuVyv4hOlte',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-15 17:55:39','2020-03-29 17:59:39'),(5,'Sanchez Roberto','driver@demo.com','$2y$10$T/jwzYDJfC8c9CdD5PbpuOKvEXlpv4.RR1jMT0PgIMT.fzeGw67JO','OuMsmU903WMcMhzAbuSFtxBekZVdXz66afifRo3YRCINi38jkXJ8rpN0FcfS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-12-15 18:49:44','2020-03-29 17:22:10'),(6,'John Doe','driver1@demo.com','$2y$10$YF0jCx2WCQtfZOq99hR8kuXsAE0KSnu5OYSomRtI9iCVguXDoDqVm','zh9mzfNO2iPtIxj6k4Jpj8flaDyOsxmlGRVUZRnJqOGBr8IuDyhb3cGoncvS',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-29 17:28:04','2020-03-29 17:28:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-22 11:45:23
