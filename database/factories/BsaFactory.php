<?php

/** @var Factory $factory */

use App\Models\Bsa;
use App\Models\Contributor;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Bsa::class, function (Faker $faker) {
    return [
        'contributor_id' => function () {
            return \factory(Contributor::class)->create()->id;
        },
    ];
});
