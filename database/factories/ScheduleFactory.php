<?php

/** @var Factory $factory */

use App\Actions\ScheduleStatus;
use App\Models\Contributor;
use App\Models\Schedule;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Schedule::class, function (Faker $faker) {
    return [
        'title'          => $faker->sentence,
        'start_date'     => now(),
        'end_date'       => now(),
        'status'         => ScheduleStatus::AVAILABLE,
        'contributor_id' => function () {
            return \factory(Contributor::class)->create()->id;
        },
    ];
});
