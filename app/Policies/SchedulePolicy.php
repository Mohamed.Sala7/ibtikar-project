<?php

namespace App\Policies;

use App\Actions\UserType;
use App\Models\User;
use App\Models\Schedule;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User  $user
     * @param  Schedule  $schedule
     * @return mixed
     */
    public function view(User $user, Schedule $schedule)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole([UserType::CONTRIBUTOR]);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  User  $user
     * @param  Schedule  $schedule
     * @return mixed
     */
    public function update(User $user, Schedule $schedule)
    {
        return $schedule->contributor->user->id == $user->id && $schedule->isAvailable();
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @param  Schedule  $schedule
     * @return mixed
     */
    public function delete(User $user, Schedule $schedule)
    {
        return $schedule->contributor->user->id == $user->id && $schedule->isAvailable();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  User  $user
     * @param  Schedule  $schedule
     * @return mixed
     */
    public function restore(User $user, Schedule $schedule)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  User  $user
     * @param  Schedule  $schedule
     * @return mixed
     */
    public function forceDelete(User $user, Schedule $schedule)
    {
        //
    }

}
