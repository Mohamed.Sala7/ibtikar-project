@component('mail::message')
Hello {{$schedule->contributor->user->name}}

{{$schedule->booking->sme->user->name}} has booked your {{$schedule->title}} Schedule


Thanks,<br>
{{ config('app.name') }}
@endcomponent
