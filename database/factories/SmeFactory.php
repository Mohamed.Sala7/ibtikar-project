<?php

/** @var Factory $factory */

use App\Models\Sme;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Sme::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return \factory(User::class)->create()->id;
        }
    ];
});
