<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bsa extends Model
{
    use UuidTrait , SoftDeletes;

    protected $guarded = [];

    public function contributor(){
        return $this->belongsTo(Contributor::class);
    }


}
