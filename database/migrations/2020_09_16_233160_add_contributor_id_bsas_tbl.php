<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddContributorIdBsasTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bsas', function (Blueprint $table) {

            $table->uuid('contributor_id')->unique()->after('id')->nullable();
            $table->foreign('contributor_id')->references('id')->on('contributors')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('bsas', function (Blueprint $table) {
            if (DB::getDriverName() !== 'sqlite') {
                $table->dropForeign('bsas_contributor_id_foreign');
            }
            $table->dropColumn('contributor_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
