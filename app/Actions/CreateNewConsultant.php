<?php


namespace App\Actions;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class CreateNewConsultant implements CreateNewUserInterface
{

    /**
     * @inheritDoc
     */
    public function create($user)
    {
        $contributor = $user->contributor()->create([]);
        $contributor->consultant()->create([]);
    }

    public function assignRole($user)
    {
        Role::firstOrCreate(
            [
                'name' => UserType::CONSULTANT,
            ],
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::CONSULTANT,
            ]
        );

        Role::firstOrCreate(
            [
                'name' => UserType::CONTRIBUTOR,
            ],
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::CONTRIBUTOR,
            ]
        );

        $user->assignRole(UserType::CONSULTANT);
        $user->assignRole(UserType::CONTRIBUTOR);
    }
}