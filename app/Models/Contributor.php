<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 *
 * @property HasMany schedules
 * @property HasOne consultant
 * @property HasOne bsa
 * @property BelongsTo user
 */

class Contributor extends Model
{
    use UuidTrait , SoftDeletes;

    /**
     * @return BelongsTo
     */
    protected function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function schedules()
    {
        return $this->hasMany(Schedule::class);
    }
    /**
     * @return HasOne
     */
    public function consultant()
    {
        return $this->hasOne(Consultant::class);
    }

    /**
     * @return HasOne
     */
    public function bsa()
    {
        return $this->hasOne(Bsa::class);
    }

}
