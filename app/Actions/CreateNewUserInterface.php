<?php


namespace App\Actions;

use App\Models\User;

interface CreateNewUserInterface
{
    /**
     * Validate and create a newly registered user.
     * @param User $user
     * @return mixed
     */
    public function create(User $user);

    public function assignRole(User $user);

}