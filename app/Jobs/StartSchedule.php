<?php

namespace App\Jobs;

use App\Actions\ScheduleStatus;
use App\Models\Schedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Throwable;

class StartSchedule implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Schedule
     */
    private $schedule;

    /**
     * Create a new job instance.
     *
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $time_now = Carbon::now();
        EndSchedule::dispatch($this->schedule)->delay(Carbon::parse($this->schedule->end_date));

        if($time_now->lt($this->schedule->start_date) || $time_now->gte($this->schedule->end_date)){
            return;
        }

        if($this->schedule->isBooked()){
            $this->schedule->update([
                'status' => ScheduleStatus::ONGOING,
            ]);
        }

    }

}
