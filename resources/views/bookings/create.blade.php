@extends('layouts.app')

@section('content')
    <div class = "container">
        <div class = "row justify-content-center">
            @if ($errors->any())
                <div class = "alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class = "col-md-10">
                <div class = "card">
                    <div class = "card-header">{{ __('main.book') }} {{__('schedule.schedule')}}</div>

                    <div class = "card-body">
                        <form method = "POST" action = "{{ route('schedules.store_book')}}">
                            @csrf

                            <input type="hidden" name="schedule_id" value="{{$schedule->id}}">
                            <div class = "form-group row">
                                <label for = "reason" class = "col-md-3 col-form-label text-md-right">
                                    {{ __('schedule.reason') }}
                                </label>

                                <div class = "col-md-7">
                                    <textarea id = "reason" type = "text" class = "form-control @error('reason') is-invalid @enderror" name = "reason"
                                              required autocomplete = "reason" autofocus rows="10">
                                        {{ old('reason') }}
                                    </textarea>
                                    @error('reason')
                                    <span class = "invalid-feedback" role = "alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class = "form-group row mb-0">
                                <div class = "col-md-6 offset-md-4">
                                    <button type = "submit" class = "btn btn-primary">
                                        {{ __('main.submit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
