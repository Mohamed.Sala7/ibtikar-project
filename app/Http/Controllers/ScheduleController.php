<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Jobs\StartSchedule;
use App\Models\Schedule;
use App\Repositories\ScheduleRepository;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class ScheduleController extends Controller
{
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepository;

    /**
     * ScheduleController constructor.
     * @param ScheduleRepository $scheduleRepository
     */
    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Schedule::class);

        return view('schedules.create');
    }

    /**
     * Store a newly created Schedule to database.
     * add this schedule to queue , dispatch in start_date
     *
     * @param CreateScheduleRequest $request
     * @return RedirectResponse
     */
    public function store(CreateScheduleRequest $request)
    {
        $input = $request->all();
        $input['end_date'] = Carbon::parse($input['start_date'])->addHour();
        $schedule = $this->scheduleRepository->create($input);

        StartSchedule::dispatch($schedule)->delay(Carbon::parse($schedule->start_date));

        return Redirect::route('schedules.user_schedules')->with('status', 'Done');
    }

    /**
     * Display the specified resource.
     *
     * @param Schedule $schedule
     * @return void
     */
    public function show(Schedule $schedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.c
     *
     * @param Schedule $schedule
     * @return Factory|\Illuminate\View\View
     * @throws AuthorizationException
     */
    public function edit(Schedule $schedule)
    {
        $this->authorize('update',$schedule);
        return view('schedules.edit',compact('schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateScheduleRequest $request
     * @param Schedule $schedule
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(UpdateScheduleRequest $request, Schedule $schedule)
    {
        $this->authorize('update',$schedule);
        $input = $request->all();
        $input['end_date'] = Carbon::parse($input['start_date'])->addHour();
        $this->scheduleRepository->update($input , $schedule);

        StartSchedule::dispatch($schedule)->delay(Carbon::parse($schedule->start_date));

        return Redirect::route('schedules.user_schedules')->with('status', 'Done');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Schedule $schedule
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Schedule $schedule)
    {
        $this->authorize('delete',$schedule);

        $this->scheduleRepository->delete($schedule);
        return Redirect::back()->with('status', 'Done');

    }

    /**
     * Display a listing of the resource for authenticated user.
     *
     * @return Factory|\Illuminate\View\View
     */
    public function userSchedule(){
        $schedules = $this->scheduleRepository->getUserSchedules();
        return view('schedules.index',compact('schedules'));
    }

}
