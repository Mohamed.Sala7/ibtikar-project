<?php

namespace Tests\Feature;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use DatabaseMigrations , RefreshDatabase;


    public function test_show_registration_form()
    {
        $response = $this->get('/register');
        $response->assertStatus(200);
        $response->assertViewIs('auth.register');
        $response->assertSee('Register');

    }

    public function test_register_user()
    {
        $user = [
            'name'                  => 'test',
            'nick_name'             => 'test',
            'email'                 => 'test@example.com',
            'password'              => 'password',
            'password_confirmation' => 'password',
            'gender'                => 'male',
        ];

        $response = $this->post('/register', $user);

        $response->assertRedirect(RouteServiceProvider::HOME);

        $this->assertDatabaseHas('users', [
            'name'  => $user['name'],
            'email' => $user['email'],
        ]);

        $user = User::where('email', $user['email'])->first();

        $this->assertAuthenticatedAs($user);

    }

}
