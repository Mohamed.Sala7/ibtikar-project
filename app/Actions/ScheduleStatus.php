<?php


namespace App\Actions;

abstract class ScheduleStatus
{
    public const AVAILABLE = 1;
    public const BOOKED    = 2;
    public const ONGOING   = 3;
    public const END       = 4;
    public const EXPIRE    = 5;

    public static function getTypes()
    {
        return [self::AVAILABLE, self::BOOKED, self::ONGOING, self::END, self::EXPIRE];
    }

    /**
     * @param $val
     * @return string
     * @throws \ReflectionException
     */
    public static function toString($val){
        $tmp = new \ReflectionClass(get_called_class());
        $a = $tmp->getConstants();
        $b = array_flip($a);

        return ucfirst(strtolower($b[$val]));
    }

}