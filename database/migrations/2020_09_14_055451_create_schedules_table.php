<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');
            $table->timestamp('start_date');
            $table->timestamp('end_date')->nullable();
            $table->enum('status', ['available','booked','ongoing','end','expire']);
            $table->uuidMorphs('scheduleable');
            $table->uuid('sme_id')->nullable();
            $table->text('session_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('sme_id')->references('id')->on('smes')->onDelete('cascade')->onUpdate('cascade');
            $table->index(['scheduleable_type','scheduleable_id','status']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('schedules');
        Schema::enableForeignKeyConstraints();
    }
}
