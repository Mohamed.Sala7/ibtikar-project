<?php

/** @var Factory $factory */

use App\Models\Consultant;
use App\Models\Contributor;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Consultant::class, function (Faker $faker) {
    return [
        'contributor_id' => function () {
            return \factory(Contributor::class)->create()->id;
        },
    ];
});
