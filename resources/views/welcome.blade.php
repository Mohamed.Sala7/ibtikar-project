@extends('layouts.app')

@section('head')
    <!-- Fonts -->
    <link href = "https://fonts.googleapis.com/css?family=Nunito:200,600" rel = "stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
@endsection

@section('title')
    {{__('main.welcome_page')}}
@endsection

@section('content')
    <div class = "content">


        @if($schedules->Count() > 0)
            <table id = "available_schedules" class = "table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{__('schedule.title')}}</th>
                    <th>{{__('schedule.start_date')}}</th>
                    <th>{{__('schedule.author')}}</th>
                    <th>{{__('schedule.book')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($schedules as $schedule)
                    <tr>
                        <td>{{$schedule->title}}</td>
                        <td>{{$schedule->start_date}}</td>
                        <td>{{$schedule->contributor->user->name}}</td>
                        <td><a class="btn btn-success" href="{{route('schedules.book_schedule',$schedule)}}">{{__('schedule.book')}}</a></td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <div class = "container">
                <div class = "row justify-content-center">
                    <div class = "col-md-8">
                        <div class = "card">
                            <div class = "card-header">{{ __('Welcome') }}</div>

                            <div class = "card-body">
                                {{ __('messages.no_Schedules_available') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
@endsection

@section('extra-scripts')

    <script>
        $(document).ready(function () {
            $('#available_schedules').DataTable();
        });
    </script>

@endsection