<?php

namespace App\Listeners;

use App\Mail\SmeBookedScheduleMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SmeHasBookedScheduleListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        Mail::to($event->schedule->contributor->user->email)->send(new SmeBookedScheduleMail($event->schedule));
    }
}
