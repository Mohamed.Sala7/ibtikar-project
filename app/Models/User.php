<?php

namespace App\Models;

use App\Actions\UserType;
use App\Traits\UuidTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 * @package App\Models
 *
 * @property HasOne contributor
 * @property HasOne sme
 *
 * @property string id
 * @property string name
 * @property string nick_name
 * @property string email
 * @property string gender
 */

class User extends Authenticatable
{
    use Notifiable , UuidTrait,HasRoles , SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','nick_name','gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * @return HasOne
     */
    public function contributor(){
        return $this->hasOne(Contributor::class);
    }

    /**
     * @return HasOne
     */
    public function sme(){
        return $this->hasOne(Sme::class);
    }

}
