<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes();

Route::resource('/schedules', 'ScheduleController');

Route::middleware('auth')->group(function () {
    Route::get('/user_schedules', 'ScheduleController@userSchedule')->name('schedules.user_schedules')->middleware('isContributor');

    Route::get('/bookings/{schedule}', 'BookingController@create')->name('schedules.book_schedule');
    Route::post('/bookings', 'BookingController@store')->name('schedules.store_book');
    Route::get('/bookings', 'BookingController@index')->name('bookings.index');
});
