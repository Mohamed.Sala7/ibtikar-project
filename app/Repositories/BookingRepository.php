<?php

namespace App\Repositories;

use App\Actions\ScheduleStatus;
use App\Actions\UserType;
use App\Models\Booking;
use App\Models\Contributor;
use App\Models\Schedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingRepository
{

    public function all(){
        return Auth::user()->sme->bookings;
    }

    public function create(array $attributes)
    {
        return Auth::user()->sme->bookings()->create($attributes);
    }

}
