<?php

namespace Tests\Feature;

use App\Actions\UserType;
use App\Models\Contributor;
use App\Models\Schedule;
use App\Models\Sme;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class ScheduleTest extends TestCase
{
    use DatabaseMigrations, RefreshDatabase;

    protected $general_schedule;

    public function setUp(): void
    {
        parent::setUp();
        $this->general_schedule = factory(Schedule::class)->create();

        Role::create(
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::CONTRIBUTOR,
            ]
        );
        Role::create(
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::SME,
            ]
        );

    }

    public function test_show_all_available_schedules()
    {
        $response = $this->get('/');
        $response->assertStatus(200)
            ->assertViewIs('welcome')
            ->assertSee($this->general_schedule->title);
    }

    public function test_an_unauthenticated_user_cant_show_user_schedules()
    {
        $this->expectException('Illuminate\Auth\AuthenticationException');
        $this->get('/user_schedules');
    }

    public function test_contributor_can_show_schedules()
    {
        $contributor = factory(Contributor::class)->create();
        $user        = $contributor->user;

        $user->assignRole(UserType::CONTRIBUTOR);
        $this->actingAs($user);

        $response = $this->get('/user_schedules');
        $response->assertStatus(200)
            ->assertViewIs('schedules.index');
    }

    public function test_sme_can_not_show_schedules()
    {
        $sme  = factory(Sme::class)->create();
        $user = $sme->user;

        $user->assignRole(UserType::SME);
        $this->actingAs($user);

        $response = $this->get('/user_schedules');
        $response->assertStatus(302)
            ->assertRedirect(RouteServiceProvider::HOME);
    }

    public function test_show_creation_form_for_new_schedule()
    {
        $contributor = factory(Contributor::class)->create();
        $user        = $contributor->user;

        $user->assignRole(UserType::CONTRIBUTOR);
        $this->actingAs($user);

        $response = $this->get('/schedules/create');
        $response->assertStatus(200)
            ->assertViewIs('schedules.create');
    }

    public function test_store_schedule()
    {
        $contributor = factory(Contributor::class)->create();
        $user        = User::find($contributor->user_id);

        $user->assignRole(UserType::CONTRIBUTOR);
        $this->actingAs($user);

        $schedule = [
            'title'      => 'test Schedule 1',
            'start_date' => now()->addHour()->toDateTimeString(),
        ];
        $response = $this->post(route('schedules.store'), $schedule);
        $response->assertRedirect(route('schedules.user_schedules'));

        $this->assertDatabaseHas('schedules', [
            'title'      => $schedule['title'],
            'start_date' => $schedule['start_date'],
        ]);

        $response = $this->get('/');
        $response->assertSee($schedule['title']);

    }


}
