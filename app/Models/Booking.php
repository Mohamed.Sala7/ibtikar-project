<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use UuidTrait , SoftDeletes;

    protected $guarded = [];

    public function sme(){
        return $this->belongsTo(Sme::class);
    }

    public function schedule(){
        return $this->belongsTo(Schedule::class);
    }


}
