<?php

namespace App\Repositories;

use App\Actions\ScheduleStatus;
use App\Actions\UserType;
use App\Models\Contributor;
use App\Models\Schedule;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleRepository
{

    /**
     *
     * get all available Schedules
     */
    public function availableSchedules()
    {
        return Schedule::where('status', ScheduleStatus::AVAILABLE)->get();
    }

    /**
     * get schedules of authenticated user
     * @return mixed
     */
    public function getUserSchedules()
    {
        return Auth::user()->contributor->schedules;
    }

    /**
     * create new Schedule when Consultant or BSA create one
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        $contributor = Auth::user()->contributor;
        return $contributor->schedules()->create($attributes);
    }

    /**
     * @param array $attributes
     * @param Schedule $schedule
     * @return bool
     */
    public function update(array $attributes, Schedule $schedule)
    {
        return $schedule->update($attributes);
    }

    /**
     * CheckSchedules Command will run this method every minute,
     * check for schedules status,
     * will call changeStatus method
     */
    public function checkStatus()
    {
        $time_now = Carbon::now();

        $available_schedules = Schedule::where('status', ScheduleStatus::available())->where('start_date', '<=', $time_now)->get();
        $this->changeStatus($available_schedules, ScheduleStatus::expire());

        $booked_schedules = Schedule::where('status', ScheduleStatus::booked())->where('start_date', '<=', $time_now)->get();
        $this->changeStatus($booked_schedules, ScheduleStatus::ongoing());

        $ongoing_schedules = Schedule::where('status', ScheduleStatus::ongoing())->where('end_date', '<=', $time_now)->get();
        $this->changeStatus($ongoing_schedules, ScheduleStatus::end());

    }

    /**
     * @param $schedules
     * @param $status
     */
    public function changeStatus($schedules, $status)
    {
        foreach ($schedules as $schedule) {
            $schedule->update([
                'status' => $status,
            ]);
        }
    }


    /**
     * creator can delete schedule when it is available
     * @param Schedule $schedule
     * @return bool|null
     * @throws Exception
     */
    public function delete(Schedule $schedule)
    {
        return $schedule->delete();
    }


    /**
     * to change status of schedule to book once SME booked it
     * @param Schedule $schedule
     * @return mixed
     */
    public function book(Schedule $schedule)
    {
        $schedule->status = ScheduleStatus::BOOKED;
        $schedule->save();
    }

}
