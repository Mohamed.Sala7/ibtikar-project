<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContributorSchedulesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedules', function (Blueprint $table) {

            $table->uuid('contributor_id')->after('id')->nullable();
            $table->foreign('contributor_id')->references('id')->on('contributors')->onDelete('cascade')->onUpdate('cascade');

            $table->index('contributor_id','status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('schedules', function (Blueprint $table) {
            if (DB::getDriverName() !== 'sqlite') {
                $table->dropForeign('schedules_contributor_id_foreign');
            }
            $table->dropColumn('contributor_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
