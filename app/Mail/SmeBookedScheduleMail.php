<?php

namespace App\Mail;

use App\Models\Schedule;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SmeBookedScheduleMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Schedule
     */
    private $schedule;

    /**
     * Create a new message instance.
     *
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        //
        $this->schedule = $schedule;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.bookScheduleMail')->with('schedule',$this->schedule);
    }
}
