<?php

namespace App\Console\Commands;

use App\Repositories\ScheduleRepository;
use Illuminate\Console\Command;

class CheckSchedules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedules:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'will run this command every one minute to check status of schedules';
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepository;

    /**
     * Create a new command instance.
     *
     * @param ScheduleRepository $scheduleRepository
     */
    public function __construct(ScheduleRepository $scheduleRepository)
    {
        parent::__construct();
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->scheduleRepository->checkStatus();
    }
}
