<?php

namespace App\Jobs;

use App\Actions\ScheduleStatus;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EndSchedule implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Schedule $schedule;

    /**
     * Create a new job instance.
     *
     * @param Schedule $schedule
     */
    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $time_now = Carbon::now();

        if($time_now->lt( $this->schedule->end_date)){
            return;
        }

        if($this->schedule->isAvailable()){
            $this->schedule->update([
                'status' => ScheduleStatus::EXPIRE,
            ]);
        }elseif($this->schedule->isOngoing()){
            $this->schedule->update([
                'status' => ScheduleStatus::END,
            ]);
        }

    }
}
