<?php

namespace App\Http\Controllers;

use App\Models\Schedule;
use App\Repositories\ScheduleRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Redis\RedisManager;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepository;

    /**
     * Create a new controller instance.
     *
     * @param ScheduleRepository $scheduleRepository
     */
    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function welcome(){
        $schedules = $this->scheduleRepository->availableSchedules();
        return view('welcome',compact('schedules'));
    }
}
