<?php

namespace App\Models;

use App\Actions\ScheduleStatus;
use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models

 * @property BelongsTo contributor
 * @property HasOne booking
 * @property boolean isAvailable
 * @property boolean isBooked
 * @property boolean isOngoing
 * @property boolean isExpire
 * @property boolean isEnd
 *
 * @property string title
 * @property string status
 */

class Schedule extends Model
{
    use UuidTrait , SoftDeletes;

    protected $guarded = [];

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    /**
     * @return BelongsTo
     */
    public function contributor(){
        return $this->belongsTo(Contributor::class);
    }

    /**
     * @return HasOne
     */
    public function booking(){
        return $this->hasOne(Booking::class);
    }


    public function isAvailable(){
        return $this->status == ScheduleStatus::AVAILABLE;
    }

    public function isBooked(){
        return $this->status == ScheduleStatus::BOOKED;
    }

    public function isOngoing(){
        return $this->status == ScheduleStatus::ONGOING;
    }

    public function isExpire(){
        return $this->status == ScheduleStatus::EXPIRE;
    }

    public function isEnd(){
        return $this->status == ScheduleStatus::END;
    }

}
