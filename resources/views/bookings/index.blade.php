@extends('layouts.app')
@section('head')

@endsection

@section('content')


    <!-- Content Header (Page header) -->
    <section class = "content-header">
        <div class = "container-fluid">
            <div class = "row mb-2">
                <div class = "col-sm-12">
                    <ol class = "breadcrumb">
                        <li class = "breadcrumb-item"><a href = "{{route('welcome')}}">
                                {{__('main.home')}}
                            </a></li>
                        <li class = "breadcrumb-item active">{{__('booking.bookings')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class = "content">

        @if( $bookings && $bookings->Count() > 0)
            <table id = "available_schedules" class = "table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{__('schedule.title')}}</th>
                    <th>{{__('schedule.start_date')}}</th>
                    <th>{{__('schedule.author')}}</th>
                    <th>{{__('schedule.status')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bookings as $booking)
                    <tr>
                        <td>{{$booking->schedule->title}}</td>
                        <td>{{$booking->schedule->start_date}}</td>
                        <td>{{$booking->schedule->contributor->user->name}}</td>
                        <td>{{\App\Actions\ScheduleStatus::toString($booking->schedule->status)}}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <div class = "container">
                <div class = "row justify-content-center">
                    <div class = "col-md-8">
                        <div class = "card">
                            <div class = "card-header">{{ __('Welcome') }}</div>

                            <div class = "card-body">
                                {{ __('messages.no_schedules_available') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>

@endsection

@section('extra-scripts')

    <script>
        $(document).ready(function () {
            $('#available_schedules').DataTable();
        });
    </script>

@endsection