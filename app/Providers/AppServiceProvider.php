<?php

namespace App\Providers;

use App\Actions\CreateNewBsa;
use App\Actions\CreateNewConsultant;
use App\Actions\CreateNewSme;
use App\Actions\CreateNewUserInterface;
use App\Actions\UserType;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CreateNewUserInterface::class, function ($app) {
            if(request()->input('user_type') == UserType::CONSULTANT){
                return new CreateNewConsultant();
            }
            elseif(request()->input('user_type') == UserType::BSA){
                return new CreateNewBsa();
            }
            else{
                return new CreateNewSme();
            }

        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
