<?php

namespace App\Http\Controllers;

use App\Models\Bsa;
use Illuminate\Http\Request;

class BsaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bsa  $bsa
     * @return \Illuminate\Http\Response
     */
    public function show(Bsa $bsa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bsa  $bsa
     * @return \Illuminate\Http\Response
     */
    public function edit(Bsa $bsa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bsa  $bsa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bsa $bsa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bsa  $bsa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bsa $bsa)
    {
        //
    }
}
