<?php


namespace App\Actions;

abstract class UserType
{
    public const CONTRIBUTOR = 'contributor';
    public const CONSULTANT = 'consultant';
    public const BSA = 'bsa';
    public const SME = 'sme';

    public static function getTypes(){
        return [self::CONSULTANT, self::BSA, self::SME];
    }

}