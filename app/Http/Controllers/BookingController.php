<?php

namespace App\Http\Controllers;

//use App\Events\SmeHasBookedScheduleEvent;
use App\Http\Requests\BookScheduleRequest;
use App\Models\Booking;
use App\Models\Schedule;
use App\Notifications\ScheduleBooked;
use App\Repositories\BookingRepository;
use App\Repositories\ScheduleRepository;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class BookingController extends Controller
{
    /**
     * @var BookingRepository
     */
    private $bookingRepository;
    /**
     * @var ScheduleRepository
     */
    private $scheduleRepository;

    /**
     * BookingController constructor.
     * @param BookingRepository $bookingRepository
     * @param ScheduleRepository $scheduleRepository
     */
    public function __construct(BookingRepository $bookingRepository , ScheduleRepository $scheduleRepository)
    {

        $this->bookingRepository = $bookingRepository;
        $this->scheduleRepository = $scheduleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $bookings = $this->bookingRepository->all();
        return view('bookings.index',compact('bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Schedule $schedule
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function create(Schedule $schedule)
    {
        $this->authorize('create',Booking::class);
        return view('bookings.create',compact('schedule'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BookScheduleRequest $request
     * @return RedirectResponse
     */
    public function store(BookScheduleRequest $request)
    {
        $input = $request->all();
        $schedule = $this->bookingRepository->create($input)->schedule;
        $this->scheduleRepository->book($schedule);

//        event(new SmeHasBookedScheduleEvent($schedule));

        $user = $schedule->contributor->user;
        $user->notify(new ScheduleBooked($schedule));


        return Redirect::route('bookings.index')->with('status', 'Done');
    }

    /**
     * Display the specified resource.
     *
     * @param Booking $booking
     * @return void
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
