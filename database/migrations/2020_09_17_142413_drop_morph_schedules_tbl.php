<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropMorphSchedulesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DB::getDriverName() === 'sqlite') {
            Schema::table('schedules', function (Blueprint $table) {
                $table->dropColumn('scheduleable_type');
            });
            Schema::table('schedules', function (Blueprint $table) {
                $table->dropColumn('scheduleable_id');
            });
        }
        else{
            Schema::table('schedules', function (Blueprint $table) {
                $table->dropIndex('schedules_scheduleable_type_scheduleable_id_status_index');
                $table->dropMorphs('scheduleable');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedules', function (Blueprint $table) {
            //
        });
    }
}
