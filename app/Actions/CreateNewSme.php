<?php


namespace App\Actions;


use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class CreateNewSme implements CreateNewUserInterface
{

    /**
     * @inheritDoc
     */
    public function create($user)
    {
        $user->sme()->create([]);
    }

    public function assignRole($user)
    {
        Role::firstOrCreate(
            [
                'name' => UserType::SME,
            ],
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::SME,
            ]
        );
        $user->assignRole(UserType::SME);
    }
}