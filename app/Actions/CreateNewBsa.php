<?php


namespace App\Actions;


use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class CreateNewBsa implements CreateNewUserInterface
{

    /**
     * @inheritDoc
     */
    public function create($user)
    {
        $contributor = $user->contributor()->create([]);
        $contributor->bsa()->create([]);
    }

    public function assignRole($user)
    {
        Role::firstOrCreate(
            [
                'name' => UserType::BSA,
            ],
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::BSA,
            ]
        );
        Role::firstOrCreate(
            [
                'name' => UserType::CONTRIBUTOR,
            ],
            [
                'id'   => (string)Str::uuid(),
                'name' => UserType::CONTRIBUTOR,
            ]
        );

        $user->assignRole(UserType::BSA);
        $user->assignRole(UserType::CONTRIBUTOR);
    }
}