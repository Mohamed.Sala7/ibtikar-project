<?php

namespace App\Http\Middleware;

use App\Actions\UserType;
use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;

class ContributorCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && auth()->user()->hasRole(UserType::CONTRIBUTOR)){
            return $next($request);
        }else{
            return redirect()->to(RouteServiceProvider::HOME);
        }
    }
}
