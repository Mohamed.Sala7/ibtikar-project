@extends('layouts.app')
@section('head')

@endsection

@section('content')


    <!-- Content Header (Page header) -->
    <section class = "content-header">
        <div class = "container-fluid">
            <div class = "row mb-2">
                <div class = "col-sm-12">
                    <ol class = "breadcrumb">
                        <li class = "breadcrumb-item"><a
                                    href = "{{route('schedules.user_schedules')}}">{{__('main.home')}}</a></li>
                        <li class = "breadcrumb-item active">{{__('schedule.schedules')}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <div class = "content">

        @can('create',\App\Models\Schedule::class)
            <div class = "container-fluid">
                <div class = "row mb-2">
                    <div class = "col-sm-12">
                        <a class = "btn btn-info" href = "{{route('schedules.create')}}">{{__('main.add_new')}}</a>
                    </div>
                </div>
            </div>
        @endcan


        @if( $schedules && $schedules->Count() > 0)
            <table id = "available_schedules" class = "table table-bordered table-striped">
                <thead>
                <tr>
                    <th>{{__('schedule.title')}}</th>
                    <th>{{__('schedule.start_date')}}</th>
                    <th>{{__('schedule.author')}}</th>
                    <th>{{__('schedule.status')}}</th>
                    @hasanyrole(\App\Actions\UserType::CONSULTANT.'|'.\App\Actions\UserType::BSA)
                    <th>{{__('schedule.action')}}</th>
                    @endhasanyrole

                </tr>
                </thead>
                <tbody>
                @foreach($schedules as $schedule)
                    <tr>
                        <td>{{$schedule->title}}</td>
                        <td>{{$schedule->start_date}}</td>
                        <td>{{$schedule->contributor->user->name}}</td>
                        <td>{{\App\Actions\ScheduleStatus::toString($schedule->status)}}</td>
                        @hasanyrole(\App\Actions\UserType::CONSULTANT.'|'.\App\Actions\UserType::BSA)
                        <td>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="btn btn-info" href="{{route('schedules.edit',$schedule)}}">Edit</a>
                                </div>
                                <div class="col-sm-6">
                                    <form action="{{route('schedules.destroy',$schedule)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>
                                </div>
                            </div>
                        </td>
                        @endhasanyrole
                    </tr>
                @endforeach

                </tbody>
            </table>
        @else
            <div class = "container">
                <div class = "row justify-content-center">
                    <div class = "col-md-8">
                        <div class = "card">
                            <div class = "card-header">{{ __('Welcome') }}</div>

                            <div class = "card-body">
                                {{ __('messages.no_schedules_available') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>

@endsection

@section('extra-scripts')

    <script>
        $(document).ready(function () {
            $('#available_schedules').DataTable();
        });
    </script>

@endsection